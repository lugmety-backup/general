<?php
/**
 * Created by PhpStorm.
 * User: ashish
 * Date: 4/11/2017
 * Time: 11:01 AM
 */

namespace App\Repo;


interface PageInterface
{
    public function getAllPage($status, $country);

    public function getAllPageForPagination($request);

    public function getSpecificPage($id, $status);

    public function getSpecificPageBySlug($country_code, $slug);

    public function createPage(array $attributes);

    public function updatePage($id, array $attributes);

    public function deletePage($id);

}