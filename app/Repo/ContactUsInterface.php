<?php
/**
 * Created by PhpStorm.
 * User: Own
 * Date: 1/11/2018
 * Time: 3:17 PM
 */

namespace App\Repo;


interface ContactUsInterface
{

    public function getAllMessage();

    public function createMessage(array $request);
}