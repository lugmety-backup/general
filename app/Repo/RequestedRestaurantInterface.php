<?php
/**
 * Created by PhpStorm.
 * User: ashish
 * Date: 4/11/2017
 * Time: 11:01 AM
 */

namespace App\Repo;


interface RequestedRestaurantInterface
{
    public function getAllRequestedRestaurant();

    public function getSpecificRequestedRestaurant($id);

    public function createRequestedRestaurant(array $attributes);

    public function updateRequestedRestaurant($id, array $attributes);

    public function deleteRequestedRestaurant($id);

}