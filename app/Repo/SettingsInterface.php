<?php
/**
 * Created by PhpStorm.
 * User: ashish
 * Date: 4/11/2017
 * Time: 11:01 AM
 */

namespace App\Repo;


interface SettingsInterface
{
    public function getAllSettings();

    public function getAllSettingsPublic();

    public function getSpecificSettings($id);

    public function getSpecificSettingsBySlug($slug);

    public function createSettings(array $attributes);

    public function updateSettings($id, array $attributes);

    public function deleteSettings($id);

}