<?php
/**
 * Created by PhpStorm.
 * User: Prashant
 * Date: 6/11/2017
 * Time: 3:19 PM
 */

namespace App\Repo\Eloquent;

use App\StaticBlockTranslation;
use App\Repo\StaticBlockTranslationInterface;

class StaticBlockTranslationRepo implements StaticBlockTranslationInterface
{
    protected $StaticBlockTranslation;
    /**
     * StaticBlockTranslationRepo constructor.
     */
    public function __construct(StaticBlockTranslation $StaticBlockTranslation)
    {
        $this->StaticBlockTranslation = $StaticBlockTranslation;
    }

    public function getAllStaticBlockTranslation($id)
    {
            return $this->StaticBlockTranslation->where('static_block_id',$id)->get();
    }

    public function getAllStaticBlockTranslationByLang($id, $lang)
    {
        $StaticBlockTranslation = $this->StaticBlockTranslation->where([['lang_code', $lang],['static_block_id', $id]])->get();
        if ($StaticBlockTranslation->count() == 0) {
            $StaticBlockTranslation = $this->StaticBlockTranslation->where([['lang_code', "en"],['static_block_id',$id]])->get();
        }
        return $StaticBlockTranslation;
    }

    public function getSpecificStaticBlockTranslation($id)
    {
        return $this->StaticBlockTranslation->findOrFail($id);
    }

    public function createStaticBlockTranslation(array $attributes)
    {
        return $this->StaticBlockTranslation->create($attributes);
    }

    public function updateStaticBlockTranslation($id, array $attributes)
    {
        return $this->StaticBlockTranslation->findOrFail($id)->update($attributes);
    }

    public function deleteStaticBlockTranslation($id)
    {
        return $this->StaticBlockTranslation->findOrFail($id)->delete();
    }
}