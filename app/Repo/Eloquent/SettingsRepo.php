<?php
/**
 * Created by PhpStorm.
 * User: Prashant
 * Date: 6/11/2017
 * Time: 3:19 PM
 */

namespace App\Repo\Eloquent;

use App\Setting;
use App\Repo\SettingsInterface;

class SettingsRepo implements SettingsInterface
{
    protected $Settings;
    /**
     * SettingsRepo constructor.
     */
    public function __construct(Setting $Settings)
    {
        $this->Settings = $Settings;
    }

    public function getAllSettings()
    {
        return $this->Settings->all();
    }

    public function getAllSettingsPublic()
    {
        return $this->Settings->where("type","public")->select('slug','name','value')->get();
    }

    public function getSpecificSettings($id)
    {
        return $this->Settings->findOrFail($id);
    }

    public function getSpecificSettingsBySlug($slug)
    {
        return $this->Settings->where('slug',$slug)->get();
    }

    public function createSettings(array $attributes)
    {
        return $this->Settings->create($attributes);
    }

    public function updateSettings($id, array $attributes)
    {
        return $this->Settings->findOrFail($id)->update($attributes);
    }

    public function deleteSettings($id)
    {
        $Settings = $this->Settings->findOrFail($id);
        return $Settings->delete();
    }
}