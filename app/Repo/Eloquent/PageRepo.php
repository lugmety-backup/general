<?php
/**
 * Created by PhpStorm.
 * User: Prashant
 * Date: 6/11/2017
 * Time: 3:19 PM
 */

namespace App\Repo\Eloquent;

use App\Page;
use App\Repo\PageInterface;

class PageRepo implements PageInterface
{
    protected $Page;
    /**
     * PageRepo constructor.
     */
    public function __construct(Page $Page)
    {
        $this->Page = $Page;
    }

    public function getAllPage($status, $country)
    {
        if($status == null){
            return $this->Page->where('country_code',$country)->get();
        }
        else{
            return $this->Page->where([['status', $status],['country_code',$country]])->get();
        }
    }

    public function getAllPageForPagination($request)
    {
        $page = $this->Page->where([['status', 1],['country_code',$request['country_code']]])->paginate($request['limit']);
        $page->appends(['limit' => $request['limit'],'lang' => $request['lang'],'country_code'=>$request['country_code']])->withPath('/cms/public/page');
        return $page;
    }

    public function getSpecificPage($id, $status)
    {
        if($status == null){
            return $this->Page->findOrFail($id);
        }else{
            return $this->Page->where([['status', $status],['id',$id]])->first();
        }

    }

    public function getSpecificPageBySlug($country_code,$slug)
    {
        return $this->Page->where([['slug', $slug],['country_code',$country_code],['status',1]])->first();
    }

    public function createPage(array $attributes)
    {
        return $this->Page->create($attributes);
    }

    public function updatePage($id, array $attributes)
    {
        return $this->Page->findOrFail($id)->update($attributes);
    }

    public function deletePage($id)
    {
        $page = $this->Page->findOrFail($id);
        if($page['status'] == 0)
            return $page->delete();
        else
            return $page->update(['status' => 0]);
    }
}