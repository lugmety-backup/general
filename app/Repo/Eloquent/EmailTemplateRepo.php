<?php
/**
 * Created by PhpStorm.
 * User: Prashant
 * Date: 6/11/2017
 * Time: 3:19 PM
 */

namespace App\Repo\Eloquent;

use App\EmailTemplate;
use App\Repo\EmailTemplateInterface;

class EmailTemplateRepo implements EmailTemplateInterface
{
    protected $emailTemplate;
    /**
     * EmailTemplateRepo constructor.
     */
    public function __construct(EmailTemplate $emailTemplate)
    {
        $this->emailTemplate = $emailTemplate;
    }

    public function getAllEmailTemplate($status)
    {
        if($status == null){
            return $this->emailTemplate->all();
        }
        else{
            return $this->emailTemplate->where('status', $status)->get();
        }
    }

    public function getAllEmailTemplateForPagination($limit)
    {
        $emailTemplate = $this->emailTemplate->where('status', 1)->paginate($limit);
        $emailTemplate->appends(['limit' => $limit])->withPath('/public/email/template');
        return $emailTemplate;
    }

    public function getSpecificEmailTemplate($id)
    {
        return $this->emailTemplate->findOrFail($id);
    }

    public function getSpecificEmailTemplateBySlug($slug)
    {
        return $this->emailTemplate->where('slug',$slug)->get();
    }

    public function createEmailTemplate(array $attributes)
    {
        return $this->emailTemplate->create($attributes);
    }

    public function updateEmailTemplate($id, array $attributes)
    {
        return $this->emailTemplate->findOrFail($id)->update($attributes);
    }

    public function deleteEmailTemplate($id)
    {
        $emailTemplate = $this->emailTemplate->findOrFail($id);
        if($emailTemplate['status'] == 0)
            return $emailTemplate->delete();
        else
            return $emailTemplate->update(['status' => 0]);
    }
}