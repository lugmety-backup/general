<?php
/**
 * Created by PhpStorm.
 * User: Own
 * Date: 2/27/2018
 * Time: 8:37 PM
 */

namespace App\Repo\Eloquent;


use App\Repo\StaticApiTranslationInterface;
use App\StaticApiTranslation;
class StaticApiTranslationRepo implements StaticApiTranslationInterface
{
        protected $staticApiTranslation;

        public function __construct(StaticApiTranslation $apiTranslation)
        {
            $this->staticApiTranslation = $apiTranslation;
        }

    public function getAllStaticApiTranslationByLang($id, $lang)
    {
        return $StaticApiTranslation = $this->staticApiTranslation->where([
            ['lang_code', $lang],
            ['static_api_id', $id]
        ])->get();
        if ($StaticApiTranslation->count() == 0) {
            $StaticApiTranslation = $this->staticApiTranslation->where([['lang_code', "en"],['static_api_id',$id]])->get();
        }
        return $StaticApiTranslation;
    }

}