<?php
/**
 * Created by PhpStorm.
 * User: Prashant
 * Date: 6/11/2017
 * Time: 3:19 PM
 */

namespace App\Repo\Eloquent;

use App\Enquiries;
use App\Repo\EnquiriesInterface;

class EnquiriesRepo implements EnquiriesInterface
{
    protected $Enquiries;
    /**
     * EnquiriesRepo constructor.
     */
    public function __construct(Enquiries $Enquiries)
    {
        $this->Enquiries = $Enquiries;
    }

    public function getAllEnquiries()
    {
        return $this->Enquiries->all();
    }

    public function getSpecificEnquiries($id)
    {
        return $this->Enquiries->findOrFail($id);
    }

    public function createEnquiries(array $attributes)
    {
        return $this->Enquiries->create($attributes);
    }

    public function updateEnquiries($id, array $attributes)
    {
        return $this->Enquiries->findOrFail($id)->update($attributes);
    }

    public function deleteEnquiries($id)
    {
        $Enquiries = $this->Enquiries->findOrFail($id);
        return $Enquiries->delete();
    }
}