<?php
/**
 * Created by PhpStorm.
 * User: Prashant
 * Date: 6/11/2017
 * Time: 3:19 PM
 */

namespace App\Repo\Eloquent;

use App\ErrorMessage;
use App\Repo\ErrorMessageInterface;

class ErrorMessageRepo implements ErrorMessageInterface
{
    protected $ErrorMessage;
    /**
     * ErrorMessageRepo constructor.
     */
    public function __construct(ErrorMessage $ErrorMessage)
    {
        $this->ErrorMessage = $ErrorMessage;
    }

    public function getAllErrorMessage()
    {
        return $this->ErrorMessage->orderBy("key","asc")->get();
    }

    public function getSpecificErrorMessage($id)
    {
        return $this->ErrorMessage->findOrFail($id);
    }

    public function getErrorMessageByKey($key)
    {
        return $this->ErrorMessage->where('key',$key)->get();
    }

    public function deleteErrorMessageByKey($key)
    {
        return $this->ErrorMessage->where('key',$key)->delete();
    }

    public function createErrorMessage(array $attributes)
    {
        return $this->ErrorMessage->create($attributes);
    }

    public function updateErrorMessage($id, array $attributes)
    {
        return $this->ErrorMessage->findOrFail($id)->update($attributes);
    }

    public function deleteErrorMessage($id)
    {
        $ErrorMessage = $this->ErrorMessage->findOrFail($id);
        return $ErrorMessage->delete();
    }
}