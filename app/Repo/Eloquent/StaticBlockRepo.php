<?php
/**
 * Created by PhpStorm.
 * User: Prashant
 * Date: 6/11/2017
 * Time: 3:19 PM
 */

namespace App\Repo\Eloquent;

use App\StaticBlock;
use App\Repo\StaticBlockInterface;

class StaticBlockRepo implements StaticBlockInterface
{
    protected $StaticBlock;
    /**
     * StaticBlockRepo constructor.
     */
    public function __construct(StaticBlock $StaticBlock)
    {
        $this->StaticBlock = $StaticBlock;
    }

    public function getAllStaticBlock($status,$country)
    {
        if($status == null){
            return $this->StaticBlock->where('country_code',$country)->get();
        }
        else{
            return $this->StaticBlock->where([['status', $status],['country_code',$country]])->get();
        }
    }
    public function getSpecificStaticBlock($id, $status)
    {
        if($status == null){
            return $this->StaticBlock->findOrFail($id);
        }else{
            return $this->StaticBlock->where([['status', $status],['id',$id]])->first();
        }

    }

    public function getAllStaticBlockForPagination($request)
    {
        $staticBlock = $this->StaticBlock->where([['status', 1],['country_code',$request['country_code']]])->paginate($request['limit']);
        $staticBlock->appends(['limit' => $request['limit'],'lang' => $request['lang'],'country_code'=>$request['country_code']])->withPath('/cms/public/static');
        return $staticBlock;
    }

    public function getSpecificStaticBlockBySlug($country_code, $slug)
    {
        return $this->StaticBlock->where([['slug', $slug],['country_code',$country_code],['status',1]])->first();
    }

    public function createStaticBlock(array $attributes)
    {
        return $this->StaticBlock->create($attributes);
    }

    public function updateStaticBlock($id, array $attributes)
    {
        return $this->StaticBlock->findOrFail($id)->update($attributes);
    }

    public function deleteStaticBlock($id)
    {
        $StaticBlock = $this->StaticBlock->findOrFail($id);
        if($StaticBlock['status'] == 0)
            return $StaticBlock->delete();
        else
            return $StaticBlock->update(['status' => 0]);
    }
}