<?php
/**
 * Created by PhpStorm.
 * User: Own
 * Date: 2/27/2018
 * Time: 8:37 PM
 */

namespace App\Repo\Eloquent;


use App\Repo\StaticApiInterface;
use App\StaticApi;
class StaticApiRepo implements StaticApiInterface
{

    protected $staticAPi;
    public function __construct(StaticApi $staticApi)
    {
        $this->staticAPi = $staticApi;
    }

    public function getSpecificStaticAPiByFieldByCountryId($countryId,$field, $value){
        return $this->staticAPi->where([
            [$field,$value],
            ["country_id",$countryId]
        ])->firstOrFail();
    }

    public function getSpecificStaticAPiByField($field, $value)
    {
        return $this->staticAPi->where($field,$value)->firstOrFail();
    }

}