<?php
/**
 * Created by PhpStorm.
 * User: Prashant
 * Date: 6/11/2017
 * Time: 3:19 PM
 */

namespace App\Repo\Eloquent;

use App\RequestedRestaurant;
use App\Repo\RequestedRestaurantInterface;

class RequestedRestaurantRepo implements RequestedRestaurantInterface
{
    protected $RequestedRestaurant;
    /**
     * RequestedRestaurantRepo constructor.
     */
    public function __construct(RequestedRestaurant $RequestedRestaurant)
    {
        $this->RequestedRestaurant = $RequestedRestaurant;
    }

    public function getAllRequestedRestaurant()
    {
        return $this->RequestedRestaurant->all()->sortBy("seen");
    }

    public function getSpecificRequestedRestaurant($id)
    {
        return $this->RequestedRestaurant->findOrFail($id);
    }

    public function createRequestedRestaurant(array $attributes)
    {
        return $this->RequestedRestaurant->create($attributes);
    }

    public function updateRequestedRestaurant($id, array $attributes)
    {
        return $this->RequestedRestaurant->findOrFail($id)->update($attributes);
    }

    public function deleteRequestedRestaurant($id)
    {
        $RequestedRestaurant = $this->RequestedRestaurant->findOrFail($id);
        return $RequestedRestaurant->delete();
    }
}

