<?php
/**
 * Created by PhpStorm.
 * User: ashish
 * Date: 4/11/2017
 * Time: 11:01 AM
 */

namespace App\Repo;


interface ErrorMessageInterface
{
    public function getAllErrorMessage();

    public function getSpecificErrorMessage($id);

    public function getErrorMessageByKey($Key);

    public function deleteErrorMessageByKey($key);

    public function createErrorMessage(array $attributes);

    public function updateErrorMessage($id, array $attributes);

    public function deleteErrorMessage($id);

}