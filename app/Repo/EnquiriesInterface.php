<?php
/**
 * Created by PhpStorm.
 * User: ashish
 * Date: 4/11/2017
 * Time: 11:01 AM
 */

namespace App\Repo;


interface EnquiriesInterface
{
    public function getAllEnquiries();

    public function getSpecificEnquiries($id);

    public function createEnquiries(array $attributes);

    public function updateEnquiries($id, array $attributes);

    public function deleteEnquiries($id);

}