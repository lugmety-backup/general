<?php
/**
 * Created by PhpStorm.
 * User: ashish
 * Date: 4/11/2017
 * Time: 11:01 AM
 */

namespace App\Repo;


interface StaticBlockInterface
{
    public function getAllStaticBlock($status,$country);

    public function getAllStaticBlockForPagination($request);

    public function getSpecificStaticBlock($id, $status);

    public function getSpecificStaticBlockBySlug($country_code, $status);

    public function createStaticBlock(array $attributes);

    public function updateStaticBlock($id, array $attributes);

    public function deleteStaticBlock($id);

}