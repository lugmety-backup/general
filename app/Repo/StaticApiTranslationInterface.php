<?php
/**
 * Created by PhpStorm.
 * User: Own
 * Date: 2/27/2018
 * Time: 8:33 PM
 */

namespace App\Repo;


interface StaticApiTranslationInterface
{
    public function getAllStaticApiTranslationByLang($id, $lang);
}