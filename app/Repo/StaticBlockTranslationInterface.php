<?php
/**
 * Created by PhpStorm.
 * User: ashish
 * Date: 4/11/2017
 * Time: 11:01 AM
 */

namespace App\Repo;


interface StaticBlockTranslationInterface
{
    public function getAllStaticBlockTranslation($id);

    public function getAllStaticBlockTranslationByLang($id, $lang);

    public function getSpecificStaticBlockTranslation($id);

    public function createStaticBlockTranslation(array $attributes);

    public function updateStaticBlockTranslation($id, array $attributes);

    public function deleteStaticBlockTranslation($id);

}