<?php
/**
 * Created by PhpStorm.
 * User: ashish
 * Date: 4/11/2017
 * Time: 11:01 AM
 */

namespace App\Repo;


interface EmailTemplateInterface
{
    public function getAllEmailTemplate($status);

    public function getAllEmailTemplateForPagination($limit);

    public function getSpecificEmailTemplate($id);

    public function getSpecificEmailTemplateBySlug($slug);

    public function createEmailTemplate(array $attributes);

    public function updateEmailTemplate($id, array $attributes);

    public function deleteEmailTemplate($id);

}