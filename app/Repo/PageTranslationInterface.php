<?php
/**
 * Created by PhpStorm.
 * User: ashish
 * Date: 4/11/2017
 * Time: 11:01 AM
 */

namespace App\Repo;


interface PageTranslationInterface
{
    public function getAllPageTranslation($id);

    public function getAllPageTranslationByLang($id, $lang);

    public function getSpecificPageTranslation($id);

    public function createPageTranslation(array $attributes);

    public function updatePageTranslation($id, array $attributes);

    public function deletePageTranslation($id);

}