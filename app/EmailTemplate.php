<?php
/**
 * Created by PhpStorm.
 * User: ashish
 * Date: 4/10/2017
 * Time: 1:05 PM
 */

namespace App;

use Illuminate\Database\Eloquent\Model;

class EmailTemplate extends Model
{
    protected $guarded=['id'];
    protected $table='email_template';
    protected $fillable=['title', 'slug', 'header', 'body', 'status'];
}