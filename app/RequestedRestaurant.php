<?php
/**
 * Created by PhpStorm.
 * User: ashish
 * Date: 4/10/2017
 * Time: 1:05 PM
 */

namespace App;

use Illuminate\Database\Eloquent\Model;

class RequestedRestaurant extends Model
{
    protected $guarded=['id'];
    protected $table='requested_restaurant';
    protected $fillable=['name', 'restaurant_name', 'country', 'city','district','email','phone','request_letter','seen'];
    protected $hidden=['updated_at'];
}