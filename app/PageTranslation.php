<?php
/**
 * Created by PhpStorm.
 * User: ashish
 * Date: 4/10/2017
 * Time: 1:05 PM
 */

namespace App;

use Illuminate\Database\Eloquent\Model;

class PageTranslation extends Model
{
    protected $guarded=['id'];
    protected $table='page_translation';
    protected $fillable=['page_id', 'lang_code', 'title', 'content', 'meta_title', 'meta_description', 'meta_keyword'];
    protected $hidden=['created_at','updated_at'];

    public function page(){
        return $this->belongsTo('App\Page','page_id');
    }
}