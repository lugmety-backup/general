<?php
/**
 * Created by PhpStorm.
 * User: ashish
 * Date: 4/10/2017
 * Time: 1:05 PM
 */

namespace App;

use Illuminate\Database\Eloquent\Model;

class StaticBlockTranslation extends Model
{
    protected $guarded=['id'];
    protected $table='static_block_translation';
    protected $fillable=['static_block_id', 'lang_code', 'title', 'content'];
    protected $hidden=['created_at','updated_at'];

    public function staticBlock(){
        return $this->belongsTo('App\StaticBlock','static_block_id');
    }
}