<?php
/**
 * Created by PhpStorm.
 * User: ashish
 * Date: 4/10/2017
 * Time: 1:05 PM
 */

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Config;

class Page extends Model
{
    protected $guarded=['id'];
    protected $table='page';
    protected $fillable=['country_code', 'status', 'name', 'slug', 'hero_banner', 'wrapper_class'];

    public function pageTranslation(){
        return $this->hasMany('App\PageTranslation', 'page_id');
    }

    public function getHeroBannerAttribute($value){
        if($value != null){
            return Config::get("config.image_service_base_url_cdn").$value;
        }
        return $value;

    }
}