<?php

namespace App\Providers;

use App\Repo\Eloquent\EmailTemplateRepo;
use App\Repo\Eloquent\EnquiriesRepo;
use App\Repo\Eloquent\ErrorMessageRepo;
use App\Repo\Eloquent\PageRepo;
use App\Repo\Eloquent\PageTranslationRepo;
use App\Repo\Eloquent\RequestedRestaurantRepo;
use App\Repo\Eloquent\SettingsRepo;
use App\Repo\Eloquent\ShippingMethodRepo;
use App\Repo\Eloquent\StaticApiRepo;
use App\Repo\Eloquent\StaticApiTranslationRepo;
use App\Repo\Eloquent\StaticBlockRepo;
use App\Repo\Eloquent\StaticBlockTranslationRepo;
use App\Repo\EmailTemplateInterface;
use App\Repo\EnquiriesInterface;
use App\Repo\ErrorMessageInterface;
use App\Repo\PageInterface;
use App\Repo\PageTranslationInterface;
use App\Repo\RequestedRestaurantInterface;
use App\Repo\SettingsInterface;
use App\Repo\ShippingMethodInterface;
use App\Repo\StaticApiInterface;
use App\Repo\StaticApiTranslationInterface;
use App\Repo\StaticBlockInterface;
use App\Repo\StaticBlockTranslationInterface;
use Illuminate\Support\ServiceProvider;
use Validator;
class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('roleChecker', 'App\Http\RoleCheckerFacade');
        $this->app->bind('remoteCall','App\Http\RemoteCallFacade');
        $this->app->bind("imageUploader",'App\Http\ImageUploadFacade');
        $this->app->bind(EmailTemplateInterface::class,EmailTemplateRepo::class);
        $this->app->bind(PageInterface::class, PageRepo::class);
        $this->app->bind(PageTranslationInterface::class, PageTranslationRepo::class);
        $this->app->bind(StaticBlockInterface::class, StaticBlockRepo::class);
        $this->app->bind(SettingsInterface::class, SettingsRepo::class);
        $this->app->bind(StaticBlockTranslationInterface::class, StaticBlockTranslationRepo::class);
//        $this->app->bind(ShippingMethodInterface::class, ShippingMethodRepo::class);
        $this->app->bind(EnquiriesInterface::class,EnquiriesRepo::class);
        $this->app->bind(RequestedRestaurantInterface::class, RequestedRestaurantRepo::class);
        $this->app->bind(ErrorMessageInterface::class, ErrorMessageRepo::class);
        $this->app->bind(StaticApiInterface::class, StaticApiRepo::class);
        $this->app->bind(StaticApiTranslationInterface::class, StaticApiTranslationRepo::class);
    }

    public function boot(){

        /**
         * takes the google cloud credential
         */
        $array = [
            "type" =>env('STACK_TYPE') ,
            "project_id" => env('STACK_PROJECT_ID'),
            "private_key_id" => env('STACK_PRIVATE_KEY_ID'),
            "private_key" => str_replace('\n', "\n", env('STACK_PRIVATE_KEY')),
            "client_email" => env('STACK_CLIENT_EMAIL'),
            "client_id" => env('STACK_CLIENT_ID'),
            "auth_uri" => env('STACK_AUTH_URI'),
            "token_uri" => env('STACK_TOKEN_URL'),
            "auth_provider_x509_cert_url" => env('STACK_AUTH_PROVIDER_x509_CERT_URL'),
            "client_x509_cert_url" => env('STACK_CLIENT_X509_CERT_URL')
        ];
        /**
         * the path to create and store the json
         */
        $path = storage_path().'/stackdriver.json';
        /**
         * open the file
         */
        $fp = fopen($path, 'w');
        /**
         * store array in json with pretty print
         */
        fwrite($fp, json_encode($array,JSON_PRETTY_PRINT ));
        fclose($fp);

        Validator::extend('domain_validation', function($attribute, $value, $parameters, $validator) {

            if(strpos($value, '@') !== false) {
                $domains = explode("@",$value);
            } else {
              return true;
            }
            if(checkdnsrr($domains[1],"MX")){
                return true;
            }
            else{
                return false;
            }
        });
    }
}
