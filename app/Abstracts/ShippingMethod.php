<?php
/**
 * Created by PhpStorm.
 * User: ashish
 * Date: 4/11/2017
 * Time: 11:01 AM
 */

namespace App\Abstracts;


abstract class ShippingMethod
{
    protected $title;
    protected $slug;
    protected $class;
    protected $status;
    protected $is_taxable;
    protected $tax_percentage;
    protected $shipping_cost;
    protected $shipping_type;
    protected $dynamic_shipping_value;

    /**
     * @return mixed
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param mixed $title
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getTaxPercentage()
    {
        return $this->tax_percentage;
    }

    /**
     * @param mixed $tax_percentage
     */
    public function setTaxPercentage($tax_percentage)
    {
        $this->tax_percentage = $tax_percentage;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * @param mixed $slug
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getClass()
    {
        return $this->class;
    }

    /**
     * @param mixed $class
     */
    public function setClass($class)
    {
        $this->class = $class;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param mixed $status
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getIsTaxable()
    {
        return $this->is_taxable;
    }

    /**
     * @param mixed $is_taxable
     */
    public function setIsTaxable($is_taxable)
    {
        $this->is_taxable = $is_taxable;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getShippingCost()
    {
        return $this->shipping_cost;
    }

    /**
     * @param mixed $shipping_cost
     */
    public function setShippingCost($shipping_cost)
    {
        $this->shipping_cost = $shipping_cost;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getShippingType()
    {
        return $this->shipping_type;
    }

    /**
     * @param mixed $shipping_type
     */
    public function setShippingType($shipping_type)
    {
        $this->shipping_type = $shipping_type;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getDynamicShippingValue()
    {
        return $this->dynamic_shipping_value;
    }

    /**
     * @param mixed $dynamic_shipping_value
     */
    public function setDynamicShippingValue($dynamic_shipping_value)
    {
        $this->dynamic_shipping_value = $dynamic_shipping_value;

        return $this;
    }


}