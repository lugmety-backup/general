<?php
/**
 * Created by PhpStorm.
 * User: Own
 * Date: 2/27/2018
 * Time: 8:29 PM
 */

namespace App;


use Illuminate\Database\Eloquent\Model;

class StaticApi extends Model
{
    protected $guarded=['id'];
    protected $table='static_api';
    protected $fillable=['country_id', 'status', 'name', 'slug','type'];

    public function staticApiTranslation(){
        return $this->hasMany('App\StaticApiTranslation','static_api_id');
    }
}