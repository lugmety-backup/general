<?php
/**
 * Created by PhpStorm.
 * User: Own
 * Date: 1/11/2018
 * Time: 3:15 PM
 */

namespace App;


use Illuminate\Database\Eloquent\Model;

class ContactUs extends Model
{
    protected $table = "contact_us";
    protected $fillable = ["full_name","address","email","phone","message"];

}