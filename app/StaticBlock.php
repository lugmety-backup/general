<?php
/**
 * Created by PhpStorm.
 * User: ashish
 * Date: 4/10/2017
 * Time: 1:05 PM
 */

namespace App;

use Illuminate\Database\Eloquent\Model;

class StaticBlock extends Model
{
    protected $guarded=['id'];
    protected $table='static_block';
    protected $fillable=['country_code', 'status', 'name', 'slug'];

    public function staticBlockTranslation(){
        return $this->hasMany('App\StaticBlockTranslation','static_block_id');
    }
}