<?php
/**
 * Created by PhpStorm.
 * User: ashish
 * Date: 4/10/2017
 * Time: 1:05 PM
 */

namespace App;

use Illuminate\Database\Eloquent\Model;

class ErrorMessage extends Model
{
    protected $guarded=['id'];
    protected $table='error_message';
    protected $fillable=['key', 'lang', 'value'];

    public $timestamps = false;
}