<?php
/**
 * Created by PhpStorm.
 * User: ashish
 * Date: 4/10/2017
 * Time: 3:30 PM
 */

namespace App\Http\Middleware;
use Closure;
use Illuminate\Support\Facades\Config;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Client;
use Validator;
use GuzzleHttp\Exception\ConnectException;
use RoleChecker;
class CheckTokenPermission
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next,$permission='')
    {


        try{

            /**
             * Bearer token
             * */
            try{
                $accessToken=$test["Authorization"]=$request->header()["authorization"][0];
                $validator=Validator::make($test,[
                    "Authorization" => "required"
                ]);
                if($validator->fails()){
                    throw new \Exception();
                }
            }
            catch (\Exception $ex){
                return response()->json([
                    "status" => "422",
                    "message" => "Authorization header parameter is required and  value should start with 'Bearer your_token'"
                ],422);
            }

            /**
             * set $accessToken into access_token index on config.php which is set temporary
             *for request
             * */
            Config::set('config.access_token',$accessToken);
            /*
             * get access token which is set previously set
             * */
            $accessToken = Config::get('config.access_token');

            /*
             * explode into array 0=> Bearer
             * 1=>token
             * */
            $explodeToken=explode(" ",$accessToken);
            $client = new Client();
            $url=Config::get('config.url');

            if(!isset($explodeToken[1])){
                $explodeToken[1]= "";
            }

            $result = $client->post("$url",[
                'headers' => [
                    'Accept' => 'application/json',
                    'Authorization' => $accessToken,
                ],
                'form_params' => [
                    'token'=>$explodeToken[1],
                    'permission'=>$permission
                ]
            ]);
            $data=json_decode((string) $result->getBody(), true);
            RoleChecker::set($data);
            return $next($request);
        }
        catch(ConnectException $ex){
            $res= ["message"=>"Error Connecting to Service","status"=>503];
            return response()->json($res,503);
        }
        catch (RequestException $e)
        {
            return response()->json([
                'status'=>'401',
                'message'=>'Unauthorized'], 401);
        }
//
    }
}