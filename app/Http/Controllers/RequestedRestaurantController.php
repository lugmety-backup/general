<?php

namespace App\Http\Controllers;

use App\Repo\RequestedRestaurantInterface;
use Bschmitt\Amqp\Facades\Amqp;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redis;
use LogStoreHelper;
use Validator;
use Cocur\Slugify\Slugify;

/**
 * Class RequestedRestaurantController
 * @package App\Http\Controllers
 */
class RequestedRestaurantController extends Controller
{
    /**
     * @var RequestedRestaurantInterface
     */
    private $RequestedRestaurant;
    /**
     * @var LogStoreHelper
     */
    private $logStoreHelper;

    /**
     * @var Slugify
     */
    private $slugify;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(RequestedRestaurantInterface $RequestedRestaurant,
                                LogStoreHelper $logStoreHelper,
                                Slugify $slugify)
    {
        $this->RequestedRestaurant = $RequestedRestaurant;
        $this->logStoreHelper = $logStoreHelper;
        $this->slugify =$slugify;
    }

    /**
     * Display all the requestedRestaurant with any requestedRestaurant type
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(){
        try{
            $RequestedRestaurant = $this->RequestedRestaurant->getAllRequestedRestaurant();
            if(count($RequestedRestaurant) == 0){
                $this->logStoreHelper->storeLogInfo(array("RequestedRestaurant",[
                    'status'=>'404',
                    'message'=>'Empty Record'
                ]));
                return response()->json([
                    'status' => '404',
                    'message' => "Empty Record"
                ], 404);
            }
        }catch (\Exception $ex){
            $this->logStoreHelper->storeLogInfo(array("RequestedRestaurant",[
                'status'=>'404',
                'message'=>'Requested Restaurant could not be found'
            ]));
            return response()->json([
                'status' => '404',
                'message' => "Requested Restaurant could not be found"
            ], 404);
        }
        $this->logStoreHelper->storeLogInfo(array("RequestedRestaurant",[
            'status'=>'200',
            'data'=>$RequestedRestaurant
        ]));
        DB::table('requested_restaurant')->where('seen', '=', 0)->update(array('seen' => 1));
        return \Datatables::of($RequestedRestaurant)->make(true);
//        return response()->json([
//            'status' => '200',
//            'data' => $RequestedRestaurant
//        ], 200);
    }

    public function countUnseen(){
        try{
            $unseen = DB::table('requested_restaurant')->where('seen', '=', 0)->get();
            $count = count($unseen);
        }catch (\Exception $ex){
            $this->logStoreHelper->storeLogInfo(array("RequestedRestaurant",[
                'status'=>'404',
                'message'=>'Requested Restaurant could not be found'
            ]));
            return response()->json([
                'status' => '404',
                'message' => "Requested Restaurant could not be found"
            ], 404);
        }
        $this->logStoreHelper->storeLogInfo(array("RequestedRestaurant",[
            'status'=>'200',
            'data'=>["count" => $count]
        ]));

        return response()->json([
            'status' => '200',
            'data' => ["count" => $count]
        ], 200);
    }

    /**
     * Display specific requestedRestaurant with given slug. If the requestedRestaurant type is private, it requires secret key to view.
     *
     * @param $slug
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id){
        try{
            $requestedRestaurant = $this->RequestedRestaurant->getSpecificRequestedRestaurant($id);
        }catch (\Exception $ex){
            $this->logStoreHelper->storeLogInfo(array("RequestedRestaurant",[
                'status'=>'404',
                'message'=>'Requested Restaurant could not be found'
            ]));
            return response()->json([
                'status' => '404',
                'message' => "Requested Restaurant could not be found"
            ], 404);
        }

        $this->logStoreHelper->storeLogInfo(array("RequestedRestaurant",[
            'status'=>'200',
            'data'=>$requestedRestaurant
        ]));
        return response()->json([
            'status' => '200',
            'data' => $requestedRestaurant
        ], 200);
    }



    /**
     * Create new RequestedRestaurant.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        /**
         * Validating the request
         */
        try{
            $this->validate($request, [
                'name'=>'required',
                'restaurant_name'=>'required',
                'country'=>'required',
                'city'=>'required',
                'district'=>'required',
                'phone'=>'required|min:4|max:19',
                'email'=>'required|email',
                'request_letter'=>'required',
            ]);
        } catch (\Exception $ex){
            $this->logStoreHelper->storeLogInfo(array("RequestedRestaurant",[
                'status'=>'422',
                'message'=>'Validation Error',
                'data' => $ex->response->original
            ]));
            return response()->json([
                'status'=>'422',
                'message'=>$ex->response->original
            ],422);
        }
        $request=$request->all();
        try{
            $RequestedRestaurant = $this->RequestedRestaurant->createRequestedRestaurant($request);
        }catch (\Exception $ex){
            $this->logStoreHelper->storeLogInfo(array("RequestedRestaurant",[
                'status'=>'404',
                'message'=>"Requested Restaurant could not be created"
            ]));
            return response()->json([
                'status'=>'404',
                'message'=>"Requested Restaurant could not be created"
            ],404);
        }
        $this->logStoreHelper->storeLogInfo(array("RequestedRestaurant",[
            'status'=>'200',
            'message'=>"Requested Restaurant created successfully"
        ]));
        return response()->json([
            'status'=>'200',
            'message'=>"Requested Restaurant created successfully"
        ],200);

    }

    /**
     * Delete Specific RequestedRestaurant.
     *
     * Note: If the status of RequestedRestaurant is 1, deletion will change the status to 1.
     * And If the status of RequestedRestaurant is 0, it will delete RequestedRestaurant permanently.
     *
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function delete($id){
        try{
            $RequestedRestaurant = $this->RequestedRestaurant->getSpecificRequestedRestaurant($id)->delete();
        }catch (ModelNotFoundException $ex){
            $this->logStoreHelper->storeLogInfo(array("RequestedRestaurant",[
                'status'=>'404',
                'message'=>'Requested Restaurant could not be found'
            ]));
            return response()->json([
                'status' => '404',
                'message' => "Requested Restaurant could not be found"
            ], 404);
        }catch (\Exception $ex){
            $this->logStoreHelper->storeLogInfo(array("RequestedRestaurant",[
                'status'=>'404',
                'message'=>'Requested Restaurant could not be found'
            ]));
            return response()->json([
                'status' => '404',
                'message' => "Requested Restaurant could not be found"
            ], 404);
        }

        $this->logStoreHelper->storeLogInfo(array("RequestedRestaurant",[
            'status'=>'200',
            'message'=> "Requested Restaurant Deleted Successfully"
        ]));
        return response()->json([
            'status' => '200',
            'message' => "RequestedRestaurant Deleted Successfully"
        ], 200);
    }
}
