<?php

namespace App\Http\Controllers;

use App\Repo\EnquiriesInterface;
use Bschmitt\Amqp\Facades\Amqp;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redis;
use LogStoreHelper;
use Validator;
use Cocur\Slugify\Slugify;

/**
 * Class enquiriesController
 * @package App\Http\Controllers
 */
class EnquiriesController extends Controller
{
    /**
     * @var enquiriesInterface
     */
    private $enquiries;
    /**
     * @var LogStoreHelper
     */
    private $logStoreHelper;

    /**
     * @var Slugify
     */
    private $slugify;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(EnquiriesInterface $enquiries,
                                LogStoreHelper $logStoreHelper,
                                Slugify $slugify)
    {
        $this->enquiries = $enquiries;
        $this->logStoreHelper = $logStoreHelper;
        $this->slugify =$slugify;
    }

    /**
     * Display all the enquiry with any enquiry type
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(){
        try{
            $enquiries = $this->enquiries->getAllEnquiries();

        }catch (\Exception $ex){
            $this->logStoreHelper->storeLogInfo(array("enquiries",[
                'status'=>'404',
                'message'=>'enquiries could not be found'
            ]));
            return response()->json([
                'status' => '404',
                'message' => "enquiries could not be found"
            ], 404);
        }
        $this->logStoreHelper->storeLogInfo(array("enquiries",[
            'status'=>'200',
            'data'=>$enquiries
        ]));
        return \Datatables::of($enquiries)->make(true);
//        return response()->json([
//            'status' => '200',
//            'data' => $enquiries
//        ], 200);
    }

    /**
     * Display specific enquiry with given slug. If the enquiry type is private, it requires secret key to view.
     *
     * @param $slug
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id){
        try{
            $enquiry = $this->enquiries->getSpecificEnquiries($id);
        }catch (\Exception $ex){
            $this->logStoreHelper->storeLogInfo(array("enquiries",[
                'status'=>'404',
                'message'=>'enquiries could not be found'
            ]));
            return response()->json([
                'status' => '404',
                'message' => "enquiries could not be found"
            ], 404);
        }

        $this->logStoreHelper->storeLogInfo(array("enquiries",[
            'status'=>'200',
            'data'=>$enquiry
        ]));
        return response()->json([
            'status' => '200',
            'data' => $enquiry
        ], 200);
    }



    /**
     * Create new enquiries.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        /**
         * Validating the request
         */
        try{
            $this->validate($request, [
                'full_name'=>'required',
                'address'=>'required',
                'phone'=>'required|digits_between:4,13',
                'email_address'=>'required',
                'message'=>'required',
            ]);
        } catch (\Exception $ex){
            $this->logStoreHelper->storeLogInfo(array("enquiries",[
                'status'=>'422',
                'message'=>'Validation Error',
                'data' => $ex->response->original
            ]));
            return response()->json([
                'status'=>'422',
                'message'=>$ex->response->original
            ],422);
        }
        $request=$request->all();
        try{
            $enquiries = $this->enquiries->createEnquiries($request);
        }catch (\Exception $ex){
            $this->logStoreHelper->storeLogInfo(array("enquiries",[
                'status'=>'404',
                'message'=>"enquiries could not be created"
            ]));
            return response()->json([
                'status'=>'404',
                'message'=>"enquiries could not be created"
            ],404);
        }
        $this->logStoreHelper->storeLogInfo(array("enquiries",[
            'status'=>'200',
            'message'=>"enquiries created successfully"
        ]));
        return response()->json([
            'status'=>'200',
            'message'=>"enquiries created successfully"
        ],200);

    }

    /**
     * Delete Specific enquiries.
     *
     * Note: If the status of enquiries is 1, deletion will change the status to 1.
     * And If the status of enquiries is 0, it will delete enquiries permanently.
     *
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function delete($id){
        try{
            $enquiries = $this->enquiries->getSpecificEnquiries($id)->delete();
        }catch (ModelNotFoundException $ex){
            $this->logStoreHelper->storeLogInfo(array("enquiries",[
                'status'=>'404',
                'message'=>'enquiries could not be found'
            ]));
            return response()->json([
                'status' => '404',
                'message' => "enquiries could not be found"
            ], 404);
        }catch (\Exception $ex){
            $this->logStoreHelper->storeLogInfo(array("enquiries",[
                'status'=>'404',
                'message'=>'enquiries could not be found'
            ]));
            return response()->json([
                'status' => '404',
                'message' => "enquiries could not be found"
            ], 404);
        }

        $this->logStoreHelper->storeLogInfo(array("enquiries",[
            'status'=>'200',
            'message'=> "enquiries Deleted Successfully"
        ]));
        return response()->json([
            'status' => '200',
            'message' => "enquiries Deleted Successfully"
        ], 200);
    }
}
