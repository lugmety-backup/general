<?php

namespace App\Http\Controllers;

use App\Http\Facades\RemoteCallFacade;
use App\Repo\StaticBlockInterface;
use App\Repo\StaticBlockTranslationInterface;
use Cocur\Slugify\Slugify;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use LogStoreHelper;
use Validator;
use RemoteCall;
use Yajra\Datatables\Facades\Datatables;

/**
 * Class StaticBlockController
 * @package App\Http\Controllers
 */
class StaticBlockController extends Controller
{
    /**
     * @var StaticBlockInterface
     */
    private $staticBlock;
    /**
     * @var LogStoreHelper
     */
    private $logStoreHelper;
    /**
     * @var StaticBlockTranslationInterface
     */
    private $staticBlockTranslation;
    /**
     * @var Slugify
     */
    private $slugify;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(StaticBlockInterface $staticBlock,
                                LogStoreHelper $logStoreHelper,
                                StaticBlockTranslationInterface $staticBlockTranslation,
                                Slugify $slugify)
    {
        $this->staticBlock = $staticBlock;
        $this->logStoreHelper = $logStoreHelper;
        $this->staticBlockTranslation = $staticBlockTranslation;
        $this->slugify = $slugify;
    }

    /**
     * View all the static block with status 1
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request){
        /**
         * Language of translation to be displayed. If the translation with given language is not available english is displayed
         */
        $request['lang'] = Input::get('lang', 'en');
        $request['limit']=Input::get("limit",10);
        /**
         * if limit param is other than integer and
         */
        if (!is_null($request['limit'])) {
            $rules = [
                "limit"=>"numeric|min:1"];
            $validator = Validator::make($request->all(), $rules);

            if ($validator->fails()) {
                $request['limit']=10;
            }
        }

        //validation of country_code field
        try {
            $this->validate($request, [
                'country_code' => 'required'
            ]);
        } catch (\Exception $e) {
            return response()->json([
                'status' => '422',
                'message' => $e->response->original
            ], 422);
        }

        $request = $request->all();
        try{
            $staticBlocks = $this->staticBlock->getAllStaticBlockForPagination($request);
            if(count($staticBlocks) == 0){
                $this->logStoreHelper->storeLogInfo(array("StaticBlock",[
                    'status'=>'404',
                    'message'=>'Empty Record'
                ]));
                return response()->json([
                    'status' => '404',
                    'message' => "Empty Record"
                ], 404);
            }
        }catch (\Exception $ex){
            $this->logStoreHelper->storeLogInfo(array("StaticBlock",[
                'status'=>'404',
                'message'=>'StaticBlock could not be found'
            ]));
            return response()->json([
                'status' => '404',
                'message' => "StaticBlock could not be found"
            ], 404);
        }
        foreach ($staticBlocks as $key => $staticBlock) {
            try {
                $staticBlockTranslation = $this->staticBlockTranslation->getAllStaticBlockTranslationByLang($staticBlock['id'],$request['lang']);
                if ($staticBlockTranslation->count() == 0) {
                    throw new \Exception();
                }
            } catch (\Exception $ex) {
                return response()->json([
                    "status" => "404",
                    "message" => "Default language english not found in database"
                ], 404);
            }
            $staticBlock['lang_code'] = $staticBlockTranslation[0]['lang_code'];
            $staticBlock['title'] = $staticBlockTranslation[0]['title'];
            $staticBlock['content'] = $staticBlockTranslation[0]['content'];
        }

        $this->logStoreHelper->storeLogInfo(array("StaticBlock",[
            'status'=>'200',
            'data'=>$staticBlocks
        ]));
        return response()->json([
            'status' => '200',
            'data' => $staticBlocks
        ], 200);
    }

    /**
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function indexAdmin(Request $request){
        /**
         * Static block with status to be displayed. If status is not given static block with both the status is displayed
         */
        $request['status'] = Input::get('status');
        /**
         * Language of translation to be displayed. If the translation with given language is not available english is displayed
         */
        $request['lang'] = Input::get('lang', 'en');
        //validation of country_code field
        try {
            $this->validate($request, [
                'country_code' => 'required'
            ]);
        } catch (\Exception $e) {
            return response()->json([
                'status' => '422',
                'message' => $e->response->original
            ], 422);
        }

        try{
            $staticBlocks = $this->staticBlock->getAllStaticBlock($request['status'],$request['country_code']);
            
        }catch (\Exception $ex){
            $this->logStoreHelper->storeLogInfo(array("StaticBlock",[
                'status'=>'404',
                'message'=>'StaticBlock could not be found'
            ]));
            return response()->json([
                'status' => '404',
                'message' => "StaticBlock could not be found"
            ], 404);
        }
        foreach ($staticBlocks as $key => $staticBlock) {
            try {
                $staticBlockTranslation = $this->staticBlockTranslation->getAllStaticBlockTranslationByLang($staticBlock['id'], $request['lang']);
                if ($staticBlockTranslation->count() == 0) {
                    throw new \Exception();
                }
            } catch (\Exception $ex) {
                return response()->json([
                    "status" => "404",
                    "message" => "Default language english not found in database"
                ], 404);
            }
            $staticBlock['lang_code'] = $staticBlockTranslation[0]['lang_code'];
            $staticBlock['title'] = $staticBlockTranslation[0]['title'];
            $staticBlock['content'] = $staticBlockTranslation[0]['content'];
        }

        $this->logStoreHelper->storeLogInfo(array("StaticBlock",[
            'status'=>'200',
            'data'=>$staticBlocks
        ]));
        return Datatables::of($staticBlocks)->make(true);

    }

    /**
     * Display specific static block with status 1 only
     *
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id){
        /**
         * Language of translation to be displayed. If the translation with given language is not available english is displayed
         */
        $lang = Input::get('lang', 'en');


        try{
            $staticBlock = $this->staticBlock->getSpecificStaticBlock($id, 1);
            if($staticBlock == null){
                throw new \Exception();
            }
        }catch (\Exception $ex){
            $this->logStoreHelper->storeLogInfo(array("StaticBlock",[
                'status'=>'404',
                'message'=>'StaticBlock could not be found'
            ]));
            return response()->json([
                'status' => '404',
                'message' => "StaticBlock could not be found"
            ], 404);
        }
        try {
            $staticBlockTranslation = $this->staticBlockTranslation->getAllStaticBlockTranslationByLang($staticBlock['id'], $lang);
            if ($staticBlockTranslation->count() == 0) {
                throw new \Exception();
            }
        } catch (\Exception $ex) {
            return response()->json([
                "status" => "404",
                "message" => "Default language english not found in database"
            ], 404);
        }
        $staticBlock['lang_code'] = $staticBlockTranslation[0]['lang_code'];
        $staticBlock['title'] = $staticBlockTranslation[0]['title'];
        $staticBlock['content'] = $staticBlockTranslation[0]['content'];

        $this->logStoreHelper->storeLogInfo(array("StaticBlock",[
            'status'=>'200',
            'data'=>$staticBlock
        ]));
        return response()->json([
            'status' => '200',
            'data' => $staticBlock
        ], 200);
    }

    /**
     * Display specific static block with status 1 only
     *
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function showBySlug($country_code,$slug){
        /**
         * Language of translation to be displayed. If the translation with given language is not available english is displayed
         */
        $lang = Input::get('lang', 'en');


        try{
            $staticBlock = $this->staticBlock->getSpecificStaticBlockBySlug($country_code, $slug);
            if($staticBlock == null){
                throw new \Exception();
            }
        }catch (\Exception $ex){
            $this->logStoreHelper->storeLogInfo(array("StaticBlock",[
                'status'=>'404',
                'message'=>'StaticBlock could not be found'
            ]));
            return response()->json([
                'status' => '404',
                'message' => "StaticBlock could not be found"
            ], 404);
        }
        try {
            $staticBlockTranslation = $this->staticBlockTranslation->getAllStaticBlockTranslationByLang($staticBlock['id'], $lang);
            if ($staticBlockTranslation->count() == 0) {
                throw new \Exception();
            }
        } catch (\Exception $ex) {
            return response()->json([
                "status" => "404",
                "message" => "Default language english not found in database"
            ], 404);
        }
        $staticBlock['lang_code'] = $staticBlockTranslation[0]['lang_code'];
        $staticBlock['title'] = $staticBlockTranslation[0]['title'];
        $staticBlock['content'] = $staticBlockTranslation[0]['content'];

        $this->logStoreHelper->storeLogInfo(array("StaticBlock",[
            'status'=>'200',
            'data'=>$staticBlock
        ]));
        return response()->json([
            'status' => '200',
            'data' => $staticBlock
        ], 200);
    }

    /**
     * Display static block with any status
     *
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function adminShow($id){
        try{
            $staticBlock = $this->staticBlock->getSpecificStaticBlock($id, null);
        }catch (ModelNotFoundException $ex){
            $this->logStoreHelper->storeLogInfo(array("StaticBlock",[
                'status'=>'404',
                'message'=>'StaticBlock could not be found'
            ]));
            return response()->json([
                'status' => '404',
                'message' => "StaticBlock could not be found"
            ], 404);
        }catch (\Exception $ex){
            $this->logStoreHelper->storeLogInfo(array("StaticBlock",[
                'status'=>'404',
                'message'=>'StaticBlock could not be found'
            ]));
            return response()->json([
                'status' => '404',
                'message' => "StaticBlock could not be found"
            ], 404);
        }

        try {
            $staticBlockTranslation = $this->staticBlockTranslation->getAllStaticBlockTranslation($staticBlock['id']);
            if ($staticBlockTranslation->count() == 0) {
                throw new \Exception();
            }
        } catch (\Exception $ex) {
            return response()->json([
                "status" => "404",
                "message" => "Default language english not found in database"
            ], 404);
        }
        $staticBlock['translation'] = $staticBlockTranslation;

        $this->logStoreHelper->storeLogInfo(array("StaticBlock",[
            'status'=>'200',
            'data'=>$staticBlock
        ]));
        return response()->json([
            'status' => '200',
            'data' => $staticBlock
        ], 200);
    }

    /**
     * Create new Static Block
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        /**
         * Slugifying the slug
         */
        $request["slug"] = $this->slugify->slugify($request['slug'],['regexp' => '/([^A-Za-z]|-)+/']);
        $message =[
            "slug.required" => "The slug may not only contain numbers,special characters and should not be empty.",
            "slug.unique_with" =>"The country must have unique slug"
        ];

        /**
         * Validating the request
         */
        try{
            $this->validate($request, [
                'country_code'=>'required',
                'name'=>'required',
                'slug'=>'required|unique_with:static_block,country_code,'.$request['country_code'],
                'status'=>'integer|min:0|max:1',
                "translation" => "required|array"
            ],$message);
         } catch (\Exception $ex){
            $this->logStoreHelper->storeLogInfo(array("StaticBlock",[
                'status'=>'404',
                'message'=>'Validation Error',
                'data' => $ex->response->original
            ]));
            return response()->json([
                'status'=>'422',
                'message'=>$ex->response->original
            ],422);
        }

        $request=$request->all();

        /**
         * Status of static block. If status is not given, default is set to 1.
         */
        $request['status'] = Input::get('status', 1);

        /**
         * Checking if the given country_code is available in country table or not
         */
        $country = RemoteCall::getSpecific("http://api.stagingapp.io/location/v1/public/country/code/".$request['country_code']);
        if($country['status']!=200) {
            $this->logStoreHelper->storeLogInfo(array("RestaurantBranchUser",[
                $country['message']
            ]));
            return response()->json(
                $country['message'], $country['status']);
        }

        DB::beginTransaction();
        try{
            $staticBlock = $this->staticBlock->createStaticBlock($request);
        }catch (\Exception $ex){
            DB::rollBack();
            $this->logStoreHelper->storeLogInfo(array("StaticBlock",[
                'status'=>'404',
                'message'=>"StaticBlock could not be created"
            ]));
            return response()->json([
                'status'=>'404',
                'message'=>"StaticBlock could not be created"
            ],404);
        }

        foreach ($request['translation'] as $key=> $translation){
            $translation['static_block_id'] = $staticBlock->id;
            $translation['lang_code'] = $key;

            $rules = [
                "lang_code" => 'required|alpha',
                "title" => "required",
                "content" => "required",
            ];

            $validator = Validator::make($translation, $rules);
            if ($validator->fails()) {
                $error = $validator->errors();
                DB::rollBack();
                $this->logStoreHelper->storeLogError(array('StaticBlock Translation', [
                    'status' => '422',
                    "message" => [$key => $error]
                ]));
                return response()->json([
                    'status' => '422',
                    "message" => [$key => $error]
                ],422);
            }

            /*
             * slugify package to slugify lang_code
             * */
            $translation['lang_code']=$this->slugify->slugify($translation['lang_code'],['regexp' => '/([^A-Za-z]|-)+/']);
            if(empty($translation['lang_code'])){
                DB::rollBack();
                $this->logStoreHelper->storeLogError(array('Restaurant Translation', [
                    'status' => '422',
                    "message" => ["lang_code"=>"The lang_code may not only contain numbers,special characters."]
                ]));
                return response()->json([
                    "status"=>"422",
                    "message"=>["lang_code"=>"The lang_code may not only contain numbers,special characters."]
                ],422);
            }

            /**
             * Checking if the given lang_code is available in country or not.
             */
            if(!$this->checkLangcode($country['message']['data']['language'],$translation['lang_code']))
            {
                DB::rollBack();
                $this->logStoreHelper->storeLogInfo(array("StaticBlock Translation", [
                    "status" => "404",
                    "message" => ["StaticBlock Translation could not be created"]
                ]));
                return response()->json([
                    "status"=>"404",
                    "message"=>["StaticBlock Translation could not be created"]
                ],404);
            }

            try{
                $createTranslation = $this->staticBlockTranslation->createStaticBlockTranslation($translation);
            }catch (\Exception $ex)
            {
                DB::rollBack();
                $this->logStoreHelper->storeLogInfo(array("StaticBlock Translation", [
                    "status" => "404",
                    "message" => ["StaticBlock Translation could not be created"]
                ]));
                return response()->json([
                    "status"=>"404",
                    "message"=>["StaticBlock Translation could not be created"]
                ],404);
            }

            $this->logStoreHelper->storeLogInfo(array("StaticBlock Translation", [
                "status" => "200",
                "message" => "StaticBlock Translation Created Successfully",
                "data" => $createTranslation
            ]));
        }

        DB::commit();
        $this->logStoreHelper->storeLogInfo(array("StaticBlock",[
            'status'=>'200',
            'message'=>"StaticBlock created successfully"
        ]));
        return response()->json([
            'status'=>'200',
            'message'=>"StaticBlock created successfully"
        ],200);

    }

    /**
     * Updating the specific static block
     *
     * @param $id
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function update($id, Request $request)
    {
        /**
         * Checking if the static block with given id is available or not
         */
        try{
            $staticBlock = $this->staticBlock->getSpecificStaticBlock($id, null);
        }catch (ModelNotFoundException $ex){
            $this->logStoreHelper->storeLogInfo(array("StaticBlock",[
                'status'=>'404',
                'message'=>"StaticBlock could not be found"
            ]));
            return response()->json([
                'status'=>'404',
                'message'=> "StaticBlock could not be found"
            ],404);
        }

        /**
         * Slugifying the slug
         */
        $request["slug"] = $this->slugify->slugify($request['slug'],['regexp' => '/([^A-Za-z]|-)+/']);
        $message =[
            "slug.required" => "The slug may not only contain numbers,special characters and should not be empty.",
            "slug.unique_with_for_update" =>"The country must have unique slug"
        ];
        /**
         * Validating the request
         */
        $value= (int)$id;
        try{
            $this->validate($request, [
                'country_code'=>'required',
                'name'=>'required',
                'slug'=>'required|unique_with_for_update:static_block,country_code,'.$request['country_code'].','.$value,
                'status'=>'integer|min:0|max:1',
                "translation" => "required|array"
            ],$message);
        } catch (\Exception $ex){
            $this->logStoreHelper->storeLogInfo(array("StaticBlock",[
                'status'=>'404',
                'message'=>'Validation Error',
                'data' => $ex->response->original
            ]));
            return response()->json([
                'status'=>'422',
                'message'=>$ex->response->original
            ],422);
        }

        $request=$request->all();
        /**
         * Status of static block. If not given, default is set to 1
         */
        $request['status'] = Input::get('status', 1);

        /**
         * Checking if the given country_code is available in country table or not
         */
        $country = RemoteCall::getSpecific("http://api.stagingapp.io/location/v1/public/country/code/".$request['country_code']);
        if($country['status']!=200) {
            $this->logStoreHelper->storeLogInfo(array("RestaurantBranchUser",[
                $country['message']
            ]));
            return response()->json(
                $country['message'], $country['status']);
        }

        DB::beginTransaction();
        try{
            $this->staticBlock->updateStaticBlock($id, $request);
        }catch (\Exception $ex){
            DB::rollBack();
            $this->logStoreHelper->storeLogInfo(array("StaticBlock",[
                'status'=>'404',
                'message'=>"StaticBlock could not be updated"
            ]));
            return response()->json([
                'status'=>'404',
                'message'=>"StaticBlock could not be upadated"
            ],404);
        }
        try{
            $deleteTranslation=$staticBlock->staticBlockTranslation()->delete();
        }catch (\Exception $ex){
            DB::rollBack();
            $this->logStoreHelper->storeLogInfo(array("StaticBlock",[
                'status'=>'404',
                'message'=>"StaticBlock could not be deleted for update"
            ]));
            return response()->json([
                'status'=>'404',
                'message'=>"StaticBlock could not be deleted for update"
            ],404);
        }

        foreach ($request['translation'] as $key=> $translation){
            $translation['static_block_id'] = $staticBlock->id;
            $translation['lang_code'] = $key;
            $rules = [
                "lang_code" => 'required|alpha',
                "title" => "required",
                "content" => "required",
            ];

            $validator = Validator::make($translation, $rules);
            if ($validator->fails()) {
                $error = $validator->errors();
                DB::rollBack();
                $this->logStoreHelper->storeLogError(array('Restaurant Translation', [
                    'status' => '422',
                    "message" => [$key => $error]
                ]));
                return response()->json([
                    'status' => '422',
                    "message" => [$key => $error]
                ],422);
            }

            /*
             * slugify package to slugify lang_code
             * */
            $translation['lang_code']=$this->slugify->slugify($translation['lang_code'],['regexp' => '/([^A-Za-z]|-)+/']);
            if(empty($translation['lang_code'])){
                DB::rollBack();
                $this->logStoreHelper->storeLogError(array('Restaurant Translation', [
                    'status' => '422',
                    "message" => ["lang_code"=>"The lang_code may not only contain numbers,special characters."]
                ]));
                return response()->json([
                    "status"=>"422",
                    "message"=>["lang_code"=>"The lang_code may not only contain numbers,special characters."]
                ],422);
            }

            /**
             * Checking if the given lang_code is available in country or not.
             */
            if(!$this->checkLangcode($country['message']['data']['language'],$translation['lang_code']))
            {
                DB::rollBack();
                $this->logStoreHelper->storeLogInfo(array("StaticBlock Translation", [
                    "status" => "404",
                    "message" => ["StaticBlock Translation could not be created"]
                ]));
                return response()->json([
                    "status"=>"404",
                    "message"=>["StaticBlock Translation could not be created"]
                ],404);
            }

            try{
                $createTranslation = $this->staticBlockTranslation->createStaticBlockTranslation($translation);
            }catch (\Exception $ex)
            {
                DB::rollBack();
                $this->logStoreHelper->storeLogInfo(array("StaticBlock Translation", [
                    "status" => "404",
                    "message" => ["StaticBlock Translation could not be created"]
                ]));
                return response()->json([
                    "status"=>"404",
                    "message"=>["StaticBlock Translation could not be created"]
                ],404);
            }

            $this->logStoreHelper->storeLogInfo(array("StaticBlock Translation", [
                "status" => "200",
                "message" => "StaticBlock Translation Updated Successfully",
                "data" => $createTranslation
            ]));
        }

        DB::commit();
        $this->logStoreHelper->storeLogInfo(array("StaticBlock",[
            'status'=>'200',
            'message'=>"StaticBlock updated successfully"
        ]));
        return response()->json([
            'status'=>'200',
            'message'=>"StaticBlock updated successfully"
        ],200);
    }

    /**
     * Deleting the specific static block. If the status of static block is 1, deletion will update the status to 1. Else If the status of page is 0, deletion will delete specific static block permanently along with its translation
     *
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function delete($id){
        try{
            $staticBlock = $this->staticBlock->deleteStaticBlock($id);
        }catch (ModelNotFoundException $ex){
            $this->logStoreHelper->storeLogInfo(array("StaticBlock",[
                'status'=>'404',
                'message'=>'StaticBlock could not be found'
            ]));
            return response()->json([
                'status' => '404',
                'message' => "StaticBlock could not be found"
            ], 404);
        }catch (\Exception $ex){
            $this->logStoreHelper->storeLogInfo(array("StaticBlock",[
                'status'=>'404',
                'message'=>'StaticBlock could not be found'
            ]));
            return response()->json([
                'status' => '404',
                'message' => "StaticBlock could not be found"
            ], 404);
        }
        $this->logStoreHelper->storeLogInfo(array("StaticBlock",[
            'status'=>'200',
            'message'=> "StaticBlock Deleted Successfully"
        ]));
        return response()->json([
            'status' => '200',
            'message' => "StaticBlock Deleted Successfully"
        ], 200);
    }

    /**
     * Checking if the given lang_code is available in country language
     *
     * @param $datas
     * @param $input
     * @return bool
     */
    public function checkLangcode($datas, $input){
        foreach ($datas as $data){
            $lang[]=$data['code'];
        }
        if (in_array($input, $lang)) {
            return true;
        }else
            return false;
    }
}
