<?php

namespace App\Http\Controllers;

use App\Http\Facades\RemoteCallFacade;
use App\Repo\PageInterface;
use App\Repo\PageTranslationInterface;
use Cocur\Slugify\Slugify;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use LogStoreHelper;
use Validator;
use RemoteCall;
use Yajra\Datatables\Facades\Datatables;

class PageController extends Controller
{
    /**
     * @var PageInterface
     */
    private $page;
    private $logStoreHelper;
    private $pageTranslation;
    private $slugify;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(PageInterface $page,
                                LogStoreHelper $logStoreHelper,
                                PageTranslationInterface $pageTranslation,
                                Slugify $slugify)
    {
        $this->page = $page;
        $this->logStoreHelper = $logStoreHelper;
        $this->pageTranslation = $pageTranslation;
        $this->slugify = $slugify;
    }

    /**
     * Display all the publicly pages with status 1 along with it's translation .
     * Translation can be chosen by passing lang parameter. If none of lang parameter is detected english is chosen.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request){
        /**
         * language to be displayed, if the language provided is not available english is displayed
         */
        $request['lang'] = Input::get('lang', 'en');
        $request['limit']=Input::get("limit",10);
        /**
         * if limit param is other than integer and
         */
        if (!is_null($request['limit'])) {
            $rules = [
                "limit"=>"numeric|min:1"];
            $validator = Validator::make($request->all(), $rules);

            if ($validator->fails()) {
                $request['limit']=10;
            }
        }
        //validation of country_code field
        try {
            $this->validate($request, [
                'country_code' => 'required'
            ]);
        } catch (\Exception $e) {
            return response()->json([
                'status' => '422',
                'message' => $e->response->original
            ], 422);
        }

        $request = $request->all();
        /**
         * Listing pages with status 1 only
         */
        try{
            $pages = $this->page->getAllPageForPagination($request);
            if(count($pages) == 0){
                $this->logStoreHelper->storeLogInfo(array("Page",[
                    'status'=>'404',
                    'message'=>'Empty Record'
                ]));
                return response()->json([
                    'status' => '404',
                    'message' => "Empty Record"
                ], 404);
            }
        }catch (\Exception $ex){
            $this->logStoreHelper->storeLogInfo(array("Page",[
                'status'=>'404',
                'message'=>'Page could not be found'
            ]));
            return response()->json([
                'status' => '404',
                'message' => "Page could not be found"
            ], 404);
        }
        /**
         * Listing Translation of pages
         */
        foreach ($pages as $key => $page) {
            try {
                $pageTranslation = $this->pageTranslation->getAllPageTranslationByLang($page['id'],$request['lang']);
                if ($pageTranslation->count() == 0) {
                    throw new \Exception();
                }
            } catch (\Exception $ex) {
                return response()->json([
                    "status" => "404",
                    "message" => "Default language english not found in database"
                ], 404);
            }
            $page['lang_code'] = $pageTranslation[0]['lang_code'];
            $page['title'] = $pageTranslation[0]['title'];
            $page['content'] = $pageTranslation[0]['content'];
            $page['meta_title'] = $pageTranslation[0]['meta_title'];
            $page['meta_description'] = $pageTranslation[0]['meta_description'];
            $page['meta_keyword'] = $pageTranslation[0]['meta_keyword'];
        }

        $this->logStoreHelper->storeLogInfo(array("Page",[
            'status'=>'200',
            'data'=>$pages
        ]));
        return response()->json([
            'status' => '200',
            'data' => $pages
        ], 200);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function indexAdmin(Request $request){
        /**
         * status of pages to be displayed. if not provided all pages with both the status is displayed
         */
        $status = Input::get('status');
        /**
         * language to be displayed, if the language provided is not available english is displayed
         */
        $lang = Input::get('lang', 'en');
        //validation of country_code field
        try {
            $this->validate($request, [
                'country_code' => 'required'
            ]);
        } catch (\Exception $e) {
            return response()->json([
                'status' => '422',
                'message' => $e->response->original
            ], 422);
        }

        try{
            $pages = $this->page->getAllPage($status, $request['country_code']);

        }catch (\Exception $ex){
            $this->logStoreHelper->storeLogInfo(array("Page",[
                'status'=>'404',
                'message'=>'Page could not be found'
            ]));
            return response()->json([
                'status' => '404',
                'message' => "Page could not be found"
            ], 404);
        }
        foreach ($pages as $key => $page) {
            try {
                $pageTranslation = $this->pageTranslation->getAllPageTranslationByLang($page['id'], $lang);
                if ($pageTranslation->count() == 0) {
                    throw new \Exception();
                }
            } catch (\Exception $ex) {
                return response()->json([
                    "status" => "404",
                    "message" => "Default language english not found in database"
                ], 404);
            }
            $page['lang_code'] = $pageTranslation[0]['lang_code'];
            $page['title'] = $pageTranslation[0]['title'];
            $page['content'] = $pageTranslation[0]['content'];
            $page['meta_title'] = $pageTranslation[0]['meta_title'];
            $page['meta_description'] = $pageTranslation[0]['meta_description'];
            $page['meta_keyword'] = $pageTranslation[0]['meta_keyword'];
        }

        $this->logStoreHelper->storeLogInfo(array("Page",[
            'status'=>'200',
            'data'=>$pages
        ]));
        return Datatables::of($pages)->make(true);
    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id){
        /**
         * language to be displayed, if the language provided is not available english is displayed
         */
        $lang = Input::get('lang', 'en');
        try{
            $page = $this->page->getSpecificPage($id, 1);
            if($page == null){
                throw new \Exception();
            }
        }catch (\Exception $ex){
            $this->logStoreHelper->storeLogInfo(array("Page",[
                'status'=>'404',
                'message'=>'Page could not be found'
            ]));
            return response()->json([
                'status' => '404',
                'message' => "Page could not be found"
            ], 404);
        }
        try {
            $pageTranslation = $this->pageTranslation->getAllPageTranslationByLang($page['id'], $lang);
            if ($pageTranslation->count() == 0) {
                throw new \Exception();
            }
        } catch (\Exception $ex) {
            return response()->json([
                "status" => "404",
                "message" => "Default language english not found in database"
            ], 404);
        }
        $page['lang_code'] = $pageTranslation[0]['lang_code'];
        $page['title'] = $pageTranslation[0]['title'];
        $page['content'] = $pageTranslation[0]['content'];
        $page['meta_title'] = $pageTranslation[0]['meta_title'];
        $page['meta_description'] = $pageTranslation[0]['meta_description'];
        $page['meta_keyword'] = $pageTranslation[0]['meta_keyword'];

        $this->logStoreHelper->storeLogInfo(array("Page",[
            'status'=>'200',
            'data'=>$page
        ]));
        return response()->json([
            'status' => '200',
            'data' => $page
        ], 200);
    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function showBySlug($country_code, $slug){
        /**
         * language to be displayed, if the language provided is not available english is displayed
         */
        $lang = Input::get('lang', 'en');
        try{
            $page = $this->page->getSpecificPageBySlug($country_code, $slug);
            if($page == null){
                throw new \Exception();
            }
        }catch (\Exception $ex){
            $this->logStoreHelper->storeLogInfo(array("Page",[
                'status'=>'404',
                'message'=>'Page could not be found'
            ]));
            return response()->json([
                'status' => '404',
                'message' => "Page could not be found"
            ], 404);
        }
        try {
            $pageTranslation = $this->pageTranslation->getAllPageTranslationByLang($page['id'], $lang);
            if ($pageTranslation->count() == 0) {
                throw new \Exception();
            }
        } catch (\Exception $ex) {
            return response()->json([
                "status" => "404",
                "message" => "Default language english not found in database"
            ], 404);
        }
        $page['lang_code'] = $pageTranslation[0]['lang_code'];
        $page['title'] = $pageTranslation[0]['title'];
        $page['content'] = $pageTranslation[0]['content'];
        $page['meta_title'] = $pageTranslation[0]['meta_title'];
        $page['meta_description'] = $pageTranslation[0]['meta_description'];
        $page['meta_keyword'] = $pageTranslation[0]['meta_keyword'];

        $this->logStoreHelper->storeLogInfo(array("Page",[
            'status'=>'200',
            'data'=>$page
        ]));
        return response()->json([
            'status' => '200',
            'data' => $page
        ], 200);
    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function adminShow($id){
        try{
            $page = $this->page->getSpecificPage($id, null);
        }catch (ModelNotFoundException $ex){
            $this->logStoreHelper->storeLogInfo(array("Page",[
                'status'=>'404',
                'message'=>'Page could not be found'
            ]));
            return response()->json([
                'status' => '404',
                'message' => "Page could not be found"
            ], 404);
        }catch (\Exception $ex){
            $this->logStoreHelper->storeLogInfo(array("Page",[
                'status'=>'404',
                'message'=>'Page could not be found'
            ]));
            return response()->json([
                'status' => '404',
                'message' => "Page could not be found"
            ], 404);
        }

        try {
            $pageTranslation = $this->pageTranslation->getAllPageTranslation($page['id']);
            if ($pageTranslation->count() == 0) {
                throw new \Exception();
            }
        } catch (\Exception $ex) {
            return response()->json([
                "status" => "404",
                "message" => "Default language english not found in database"
            ], 404);
        }
        $page['translation'] = $pageTranslation;

        $this->logStoreHelper->storeLogInfo(array("Page",[
            'status'=>'200',
            'data'=>$page
        ]));
        return response()->json([
            'status' => '200',
            'data' => $page
        ], 200);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        /**
         * slugifying the slug
         */
        $request["slug"] = $this->slugify->slugify($request['slug'],['regexp' => '/([^A-Za-z]|-)+/']);
        $message =[
            "slug.required" => "The slug may not only contain numbers,special characters and should not be empty.",
            "slug.unique_with" =>"The country must have unique slug"
        ];
        /**
         * validating the request
         */
        try{
            $this->validate($request, [
                'country_code'=>'required',
                'name'=>'required',
                'slug'=>'required|unique_with:static_block,country_code,'.$request['country_code'],
                'status'=>'integer|min:0|max:1',
                "translation" => "required|array",
            ],$message);
        } catch (\Exception $ex){
            $this->logStoreHelper->storeLogInfo(array("Page",[
                'status'=>'404',
                'message'=>'Validation Error',
                'data' => $ex->response->original
            ]));
            return response()->json([
                'status'=>'422',
                'message'=>$ex->response->original
            ],422);
        }
        $request_for_image = $request;
        $request=$request->all();

        /**
         * status of page. if not provided default is set to 1
         */
        $request['status'] = Input::get('status', 1);

        /**
         * checking if the given country code is available in country table or not
         */
        $country = RemoteCall::getSpecific("http://api.stagingapp.io/location/v1/public/country/code/".$request['country_code']);
        if($country['status']!=200) {
            $this->logStoreHelper->storeLogInfo(array("RestaurantBranchUser",[
                $country['message']
            ]));
            return response()->json(
                $country['message'], $country['status']);
        }

        try{
            $Page = $this->page->getSpecificPageBySlug($request['country_code'],$request['slug']);
            if(count($Page) != 0){
                throw new \Exception();
            }
        }catch (\Exception $ex){
            $this->logStoreHelper->storeLogInfo(array("Page",[
                'status'=>'409',
                'message'=> 'Page Already Exists'
            ]));
            return response()->json([
                'status'=>'409',
                'message'=> "Page Already Exists"
            ],409);
        }
        DB::beginTransaction();
        /**
         * validating hero_banner image
         */
        if ($request_for_image->has('hero_banner')) {
            if(strlen($request_for_image->hero_banner)>10){
                $image['avatar'] = $request_for_image->hero_banner;
                $image["type"] = "logo";
                $imageUpload = \ImageUploader::upload($image);
                if ($imageUpload["status"] != 200) {
                    DB::rollBack();
                    return response()->json(
                        $imageUpload["message"]
                        , $imageUpload["status"]);
                }
                $request['hero_banner'] = $imageUpload["message"]["data"];
            }else{
                $request['hero_banner'] = "";
            }

        }

        try{
            $page = $this->page->createPage($request);
        }catch (\Exception $ex){
            DB::rollBack();
            $this->logStoreHelper->storeLogInfo(array("Page",[
                'status'=>'404',
                'message'=>"Page could not be created"
            ]));
            return response()->json([
                'status'=>'404',
                'message'=>"Page could not be created"
            ],404);
        }

        foreach ($request['translation'] as $key=> $translation){
            $translation['page_id'] = $page->id;
            $translation['lang_code'] = $key;

            $rules = [
                "lang_code" => 'required|alpha',
                "title" => "required",
                "content" => "required",
            ];

            $validator = Validator::make($translation, $rules);
            if ($validator->fails()) {
                $error = $validator->errors();
                DB::rollBack();
                $this->logStoreHelper->storeLogError(array('Page Translation', [
                    'status' => '422',
                    "message" => [$key => $error]
                ]));
                return response()->json([
                    'status' => '422',
                    "message" => [$key => $error]
                ],422);
            }

            /**
             * slugifying the lang code
             */
            $translation['lang_code']=$this->slugify->slugify($translation['lang_code'],['regexp' => '/([^A-Za-z]|-)+/']);
            if(empty($translation['lang_code'])){
                DB::rollBack();
                $this->logStoreHelper->storeLogError(array('Restaurant Translation', [
                    'status' => '422',
                    "message" => ["lang_code"=>"The lang_code may not only contain numbers,special characters."]
                ]));
                return response()->json([
                    "status"=>"422",
                    "message"=>["lang_code"=>"The lang_code may not only contain numbers,special characters."]
                ],422);
            }

            /**
             * checking if the given language in translation is available in country.
             */
            if(!$this->checkLangcode($country['message']['data']['language'],$translation['lang_code']))
            {
                DB::rollBack();
                $this->logStoreHelper->storeLogInfo(array("Page Translation", [
                    "status" => "404",
                    "message" => ["Page Translation could not be created"]
                ]));
                return response()->json([
                    "status"=>"404",
                    "message"=>["Page Translation could not be created"]
                ],404);
            }

            try{
                $createTranslation = $this->pageTranslation->createPageTranslation($translation);
            }catch (\Exception $ex)
            {
                DB::rollBack();
                $this->logStoreHelper->storeLogInfo(array("Page Translation", [
                    "status" => "404",
                    "message" => ["Page Translation could not be created"]
                ]));
                return response()->json([
                    "status"=>"404",
                    "message"=>["Page Translation could not be created"]
                ],404);
            }

            $this->logStoreHelper->storeLogInfo(array("Page Translation", [
                "status" => "200",
                "message" => "Page Translation Created Successfully",
                "data" => $createTranslation
            ]));
        }

        DB::commit();
        $this->logStoreHelper->storeLogInfo(array("Page",[
            'status'=>'200',
            'message'=>"Page created successfully"
        ]));
        return response()->json([
            'status'=>'200',
            'message'=>"Page created successfully"
        ],200);

    }

    /**
     * @param $id
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function update($id, Request $request)
    {
        /**
         * checking if the page with given id is valid or not
         */
        try{
            $page = $this->page->getSpecificPage($id, null);
        }catch (ModelNotFoundException $ex){
            $this->logStoreHelper->storeLogInfo(array("Page",[
                'status'=>'404',
                'message'=>"Page could not be found"
            ]));
            return response()->json([
                'status'=>'404',
                'message'=> "Page could not be found"
            ],404);
        }

        /**
         * slugifying the slug
         */
        $request["slug"] = $this->slugify->slugify($request['slug'],['regexp' => '/([^A-Za-z]|-)+/']);
        $message =[
            "slug.required" => "The slug may not only contain numbers,special characters and should not be empty.",
            "slug.unique_with_for_update" =>"The country must have unique slug"
        ];
        $value= (int)$id;
        /**
         * Validating the request
         */
        try{
            $this->validate($request, [
                'country_code'=>'required',
                'name'=>'required',
                'slug'=>'required|unique_with_for_update:static_block,country_code,'.$request['country_code'].','.$value,
                'status'=>'integer|min:0|max:1',
                "translation" => "required|array",
            ],$message);
        } catch (\Exception $ex){
            $this->logStoreHelper->storeLogInfo(array("Page",[
                'status'=>'404',
                'message'=>'Validation Error',
                'data' => $ex->response->original
            ]));
            return response()->json([
                'status'=>'422',
                'message'=>$ex->response->original
            ],422);
        }

        $request_for_image = $request;
        $request=$request->all();
        /**
         * status of Page . If not provided, default is set to 1.
         */
        $request['status'] = Input::get('status', 1);

        /**
         * Checking if the given country_code is available in country table.
         */
        $country = RemoteCall::getSpecific("http://api.stagingapp.io/location/v1/public/country/code/".$request['country_code']);
        if($country['status']!=200) {
            $this->logStoreHelper->storeLogInfo(array("RestaurantBranchUser",[
                $country['message']
            ]));
            return response()->json(
                $country['message'], $country['status']);
        }

        DB::beginTransaction();
        /**
         * validating hero_banner image
         */
        if ($request_for_image->has('hero_banner')) {
            if(strlen($request_for_image->hero_banner)>10){
                $image['avatar'] = $request_for_image->hero_banner;
                $image["type"] = "logo";
                $imageUpload = \ImageUploader::upload($image);
                if ($imageUpload["status"] != 200) {
                    DB::rollBack();
                    return response()->json(
                        $imageUpload["message"]
                        , $imageUpload["status"]);
                }
                $request['hero_banner'] = $imageUpload["message"]["data"];
            }else{
                $request['hero_banner'] = "";
            }

        }
        try{
            $this->page->updatePage($id, $request);
        }catch (\Exception $ex){
            DB::rollBack();
            $this->logStoreHelper->storeLogInfo(array("Page",[
                'status'=>'404',
                'message'=>"Page could not be updated"
            ]));
            return response()->json([
                'status'=>'404',
                'message'=>"Page could not be upadated"
            ],404);
        }
        try{
            $deleteTranslation=$page->pageTranslation()->delete();
        }catch (\Exception $ex){
            DB::rollBack();
            $this->logStoreHelper->storeLogInfo(array("Page",[
                'status'=>'404',
                'message'=>"Page could not be deleted for update"
            ]));
            return response()->json([
                'status'=>'404',
                'message'=>"Page could not be deleted for update"
            ],404);
        }

        foreach ($request['translation'] as $key=> $translation){
            $translation['page_id'] = $page->id;
            $translation['lang_code'] = $key;
            $rules = [
                "lang_code" => 'required|alpha',
                "title" => "required",
                "content" => "required",
            ];
            /*
             * slugifying lang_code
             * */
            $translation['lang_code']=$this->slugify->slugify($translation['lang_code'],['regexp' => '/([^A-Za-z]|-)+/']);
            if(empty($translation['lang_code'])){
                DB::rollBack();
                $this->logStoreHelper->storeLogError(array('Restaurant Translation', [
                    'status' => '422',
                    "message" => ["lang_code"=>"The lang_code may not only contain numbers,special characters."]
                ]));
                return response()->json([
                    "status"=>"422",
                    "message"=>["lang_code"=>"The lang_code may not only contain numbers,special characters."]
                ],422);
            }
            $validator = Validator::make($translation, $rules);
            if ($validator->fails()) {
                $error = $validator->errors();
                DB::rollBack();
                $this->logStoreHelper->storeLogError(array('Restaurant Translation', [
                    'status' => '422',
                    "message" => [$key => $error]
                ]));
                return response()->json([
                    'status' => '422',
                    "message" => [$key => $error]
                ],422);
            }

            /**
             * Checking if the given lang_code is available in given country with country_code
             */
            if(!$this->checkLangcode($country['message']['data']['language'],$translation['lang_code']))
            {
                DB::rollBack();
                $this->logStoreHelper->storeLogInfo(array("Page Translation", [
                    "status" => "404",
                    "message" => ["Page Translation could not be created"]
                ]));
                return response()->json([
                    "status"=>"404",
                    "message"=>["Page Translation could not be created"]
                ],404);
            }

            try{
                $createTranslation = $this->pageTranslation->createPageTranslation($translation);
            }catch (\Exception $ex)
            {
                DB::rollBack();
                $this->logStoreHelper->storeLogInfo(array("Page Translation", [
                    "status" => "404",
                    "message" => ["Page Translation could not be created"]
                ]));
                return response()->json([
                    "status"=>"404",
                    "message"=>["Page Translation could not be created"]
                ],404);
            }

            $this->logStoreHelper->storeLogInfo(array("Page Translation", [
                "status" => "200",
                "message" => "Page Translation Updated Successfully",
                "data" => $createTranslation
            ]));
        }

        DB::commit();
        $this->logStoreHelper->storeLogInfo(array("Page",[
            'status'=>'200',
            'message'=>"Page updated successfully"
        ]));
        return response()->json([
            'status'=>'200',
            'message'=>"Page updated successfully"
        ],200);
    }

    /**
     * Deleting the specific page. If the status of page is 1, deletion will update the status to 1. Else If the status of page is 0, deletion will delete specific page permanently along with its translation.
     *
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function delete($id){
        try{
            $Page = $this->page->deletePage($id);
        }catch (ModelNotFoundException $ex){
            $this->logStoreHelper->storeLogInfo(array("Page",[
                'status'=>'404',
                'message'=>'Page could not be found'
            ]));
            return response()->json([
                'status' => '404',
                'message' => "Page could not be found"
            ], 404);
        }catch (\Exception $ex){
            $this->logStoreHelper->storeLogInfo(array("Page",[
                'status'=>'404',
                'message'=>'Page could not be found'
            ]));
            return response()->json([
                'status' => '404',
                'message' => "Page could not be found"
            ], 404);
        }
        $this->logStoreHelper->storeLogInfo(array("Page",[
            'status'=>'200',
            'message'=> "Page Deleted Successfully"
        ]));
        return response()->json([
            'status' => '200',
            'message' => "Page Deleted Successfully"
        ], 200);
    }

    /**
     * Checking if the given lang_code is available in country language
     *
     * @param $datas
     * @param $input
     * @return bool
     */
    public function checkLangcode($datas,$input){
        foreach ($datas as $data){
            $lang[]=$data['code'];
        }
        if (in_array($input, $lang)) {
            return true;
        }else
            return false;
    }
}
