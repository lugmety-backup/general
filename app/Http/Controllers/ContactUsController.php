<?php
/**
 * Created by PhpStorm.
 * User: Own
 * Date: 1/11/2018
 * Time: 11:14 AM
 */

namespace App\Http\Controllers;


use App\Events\SendMessage;
use App\Http\RemoteCallFacade;
use App\Setting;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Log;
use LogStoreHelper;
class ContactUsController extends Controller
{

    protected $logStoreHelper;
    protected $contactUs;

    public function __construct(LogStoreHelper $logStoreHelper)
    {
        $this->logStoreHelper = $logStoreHelper;
    }


    public function sendMessage(Request $request)
    {
        try{
            $message =[
                "domain_validation" => "The mail server for domain does not exist."
            ];
            $this->validate($request,[
                "full_name" => "required|string",
                "email" => "required|email|domain_validation|max:255",
                "message" => "required",
                "phone" => "required|min:9|max:20",
                "address" => "required|string",
                "branch_id" => 'integer|min:1'
            ],$message);
        }
        catch (\Exception $ex){
            return response()->json([
                "status" => "422",
                "message" => $ex->response->original
            ],422);
        }
        if($request->has('branch_id')){
            $message = "contact us operator";
        }
        else{
            $message = "contact us customer";
        }
        $array = [
            'service' => 'general service',
            'message' => $message,
            'data' => [
                $request->all()
            ]
        ];

       event( new SendMessage($array));

        return response()->json([
            "status" => "200",
            "message" => "Thank you for your feedback."
        ]);

//        $apiKey =  Setting::where("slug","mail-api-key")->first();
//        if(!$apiKey){
//            return response()->json([
//                "status" => "404",
//                "message" => "Mail key could not found."
//            ],404);
//        }
//        $key = $apiKey->value;
//        $template = "contact-us";
//        $oauthBaseUrl = Config::get('config.oauth_service_url');
//        $fullUrl = $oauthBaseUrl."/notification/info";
//        $getAdminEmail = \RemoteCall::getSpecificWithoutToken($fullUrl);
//        $user["email"] = $getAdminEmail["message"]["data"]["admin"];
//        $sender = $request->all();
//        $this->sendMessageBySendGrid($template,$user,$sender,$key);
//        return response()->json([
//            "status" => "200",
//            "message" => "Thank you for your feedback."
//        ]);
    }


//    private function sendMessageBySendGrid($template, $user, $sender,$key)
//    {
//
//        $apiKey = $key;
//        $value = view($template)->with(["contactUs" => $sender]);
//        $value = (string)$value;
//        $title = "Contact us";
//        if (!is_array($user["email"])) {
//            $user["email"] = array($user["email"]);
//        }
//        $adminEmail = [];
//        foreach($user["email"] as $email){
//            $adminEmail[0]["email"] = $email["email"];
//        }
//
////        foreach ($user["email"] as $email) {
//        $request_body = array(
//            "personalizations" => array(array(
//                "to" =>$adminEmail,
//                "subject" => $title
//            )),
//            "from" => array("name" => $sender["full_name"], "email" => $sender["email"]),
//            "content" => array(array("type" => "text/html", "value" => $value))
//        );
//
//
//        $sg = new \SendGrid($apiKey);
//
//        $response = $sg->client->mail()->send()->post($request_body);
//        $statusCode = $response->statusCode();
//        Log::info("$statusCode");
////        }
//
//        return true;
//    }


}