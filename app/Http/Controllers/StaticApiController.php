<?php
/**
 * Created by PhpStorm.
 * User: Own
 * Date: 2/27/2018
 * Time: 8:41 PM
 */

namespace App\Http\Controllers;


use App\Repo\StaticApiInterface;
use App\Repo\StaticApiTranslationInterface;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Facades\Input;

class StaticApiController extends Controller
{
    protected  $staticApi;
    protected $staticApiTranslation;
    protected  $log;
    public function __construct(StaticApiInterface $staticApi,\LogStoreHelper $log, StaticApiTranslationInterface $staticApiTranslation)
    {
        $this->staticApi = $staticApi;
        $this->staticApiTranslation = $staticApiTranslation;
        $this->log = $log;
    }

    public function getSpecificStaticApi($countryId,$value){
        try{
            $lang = Input::get('lang', 'en');
            if(is_numeric($value)){
                $staticApiData = $this->staticApi->getSpecificStaticAPiByFieldByCountryId($countryId,"id",$value);
            }
            else{
                $staticApiData = $this->staticApi->getSpecificStaticAPiByFieldByCountryId($countryId,"slug",$value);
            }
            try {
                $staticApiTranslation = $this->staticApiTranslation->getAllStaticApiTranslationByLang($staticApiData['id'], $lang);
                if ($staticApiTranslation->count() == 0) {
                    throw new \Exception();
                }
            } catch (\Exception $ex) {
                return response()->json([
                    "status" => "404",
                    "message" => "Default language english not found in database"
                ], 404);
            }
            $staticApiData['lang'] = $staticApiTranslation[0]['lang_code'];
            if($staticApiData->type === "json"){
                $staticApiData["payload"] = json_decode((string)$staticApiTranslation[0]['content'],true);
                if ($staticApiData["payload"] === null && json_last_error() !== JSON_ERROR_NONE) {
                    return response()->json([
                        "status" => "422",
                        "message" => "Invalid json format."
                    ],422);
                }
            }
            else{
                $staticApiData["payload"] = $staticApiTranslation[0]['content'];
            }
            return response()->json([
                "status" => "200",
                "data" => $staticApiData
            ]);



        }
        catch (ModelNotFoundException $ex){
            return response()->json([
                "status" => "404",
                "message" => "Static Api could not be found"
            ],404);
        }
        catch (\Exception $ex){
            $this->log->storeLogInfo(["Error displaying static block",[
                "data" => $ex->getMessage()
            ]]);
            return response()->json([
                "status" => "500",
                "message" => "Error viewing static api"
            ],500);
        }
    }
}