<?php

namespace App\Http\Controllers;

use App\Repo\SettingsInterface;
use Bschmitt\Amqp\Facades\Amqp;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redis;
use LogStoreHelper;
use Validator;
use Cocur\Slugify\Slugify;

/**
 * Class SettingsController
 * @package App\Http\Controllers
 */
class SettingsController extends Controller
{
    /**
     * @var SettingsInterface
     */
    private $settings;
    /**
     * @var LogStoreHelper
     */
    private $logStoreHelper;

    /**
     * @var Slugify
     */
    private $slugify;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(SettingsInterface $settings,
                                LogStoreHelper $logStoreHelper,
                                Slugify $slugify)
    {
        $this->settings = $settings;
        $this->logStoreHelper = $logStoreHelper;
        $this->slugify =$slugify;
    }

    /**
     * Display all the setting with any setting type
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function indexAdmin(){
        try{
            $settings = $this->settings->getAllSettings();

        }catch (\Exception $ex){
            $this->logStoreHelper->storeLogInfo(array("Settings",[
                'status'=>'404',
                'message'=>'Settings could not be found'
            ]));
            return response()->json([
                'status' => '404',
                'message' => "Settings could not be found"
            ], 404);
        }
        foreach ($settings as $setting){
            $unserialized_data=@unserialize($setting['value']);
            if($unserialized_data == true){
                $data=unserialize($setting['value']);
                $data=implode(",",$data);
                $setting['value']=$data;
            }
        }
        $this->logStoreHelper->storeLogInfo(array("Settings",[
            'status'=>'200',
            'data'=>$settings
        ]));
        return \Datatables::of($settings)->make(true);
//        return response()->json([
//            'status' => '200',
//            'data' => $settings
//        ], 200);
    }

    /**
     * display setting with type public only
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function indexPublic(){
        try{
            $settings = $this->settings->getAllSettingsPublic();
            if(count($settings) == 0){
                $this->logStoreHelper->storeLogInfo(array("Settings",[
                    'status'=>'404',
                    'message'=>'Empty Record'
                ]));
                return response()->json([
                    'status' => '404',
                    'message' => "Empty Record"
                ], 404);
            }
        }catch (\Exception $ex){
            $this->logStoreHelper->storeLogInfo(array("Settings",[
                'status'=>'404',
                'message'=>'Settings could not be found'
            ]));
            return response()->json([
                'status' => '404',
                'message' => "Settings could not be found"
            ], 404);
        }
        foreach ($settings as $setting){
            $unserialized_data=@unserialize($setting['value']);
            if($unserialized_data == true){
                $data=unserialize($setting['value']);
                $data=implode(",",$data);
                $setting['value']=$data;
            }
        }
        $this->logStoreHelper->storeLogInfo(array("Settings",[
            'status'=>'200',
            'data'=>$settings
        ]));

        return response()->json([
            'status' => '200',
            'data' => $settings
        ], 200);
    }


    /**
     * Display specific setting with given slug. If the setting type is private, it requires secret key to view.
     *
     * @param $slug
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($slug, Request $request){
        try{
            $settings = $this->settings->getSpecificSettingsBySlug($slug);
            $secret = $this->settings->getSpecificSettingsBySlug('secret');
            if(count($settings) == 0){
                throw new \Exception();
            }
        }catch (\Exception $ex){
            $this->logStoreHelper->storeLogInfo(array("Settings",[
                'status'=>'404',
                'message'=>'Settings could not be found'
            ]));
            return response()->json([
                'status' => '404',
                'message' => "Settings could not be found"
            ], 404);
        }
        $setting = $settings[0];
        if(stripos($setting["slug"], "invoice-bank") !== false){
            $setting->enableArray = true;
        }
        if($setting['slug'] == "secret"){
            $this->logStoreHelper->storeLogInfo(array("Settings",[
                'status'=>'401',
                'message'=>'Unauthorized'
            ]));
            return response()->json([
                'status' => '401',
                'message' => "Unauthorized"
            ], 401);
        }
        elseif($setting['type'] == "private"){
            $request['secret'] = Input::get("secret");
            try{
                $this->validate($request, [
                    'secret'=>'required'
                ]);
            } catch (\Exception $ex){
                $this->logStoreHelper->storeLogInfo(array("Settings",[
                    'status'=>'422',
                    'message'=>'Validation Error',
                    'data' => $ex->response->original
                ]));
                return response()->json([
                    'status'=>'422',
                    'message'=>$ex->response->original
                ],422);
            }
            if($request['secret'] != $secret[0]['value']){
                $this->logStoreHelper->storeLogInfo(array("Settings",[
                    'status'=>'401',
                    'message'=>'Invalid Secret Key'
                ]));
                return response()->json([
                    'status'=>'401',
                    'message'=>'Invalid Secret Key'
                ],401);
            }
        }

        $result['slug'] = $setting['slug'];
        $result['name'] = $setting['name'];
        $result['value'] = $setting['value'];

        $this->logStoreHelper->storeLogInfo(array("Settings",[
            'status'=>'200',
            'data'=>$result
        ]));
        return response()->json([
            'status' => '200',
            'data' => $result
        ], 200);
    }

    /**
     * Display any setting by id.
     *
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function showAdmin($id){
        try{
            $setting = $this->settings->getSpecificSettings($id);
            $setting->enableArray = true;
        }catch (\Exception $ex){
            $this->logStoreHelper->storeLogInfo(array("Settings",[
                'status'=>'404',
                'message'=>'Settings could not be found'
            ]));
            return response()->json([
                'status' => '404',
                'message' => "Settings could not be found"
            ], 404);
        }

        $this->logStoreHelper->storeLogInfo(array("Settings",[
            'status'=>'200',
            'data'=>$setting
        ]));
        return response()->json([
            'status' => '200',
            'data' => $setting
        ], 200);
    }

    /**
     * Create new Settings.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        /**
         * Slugifying the slug
         */
        $request["slug"] = $this->slugify->slugify($request['slug'],['regexp' => '/([^A-Za-z]|-)+/']);
        $message =[
            "slug.required" => "The slug may not only contain numbers,special characters and should not be empty."

        ];
        DB::beginTransaction();
        /**
         * Validating the request
         */
        try{
            $this->validate($request, [
                'name'=>'required',
                'slug'=>'required|unique:settings',
                'value'=>'required',
                'type'=>'required|in:public,private',
            ],$message);
        } catch (\Exception $ex){
            $this->logStoreHelper->storeLogInfo(array("Settings",[
                'status'=>'422',
                'message'=>'Validation Error',
                'data' => $ex->response->original
            ]));
            return response()->json([
                'status'=>'422',
                'message'=>$ex->response->original
            ],422);
        }
        $request=$request->all();
        if($request['slug'] == "cover-image"){
            $image['avatar'] = $request["value"];
            $image["type"] = "logo";
            $imageUpload = \ImageUploader::upload($image);
            if ($imageUpload["status"] != 200) {
                return response()->json(
                    $imageUpload["message"]
                    , $imageUpload["status"]);
            }
            $request['value'] = $imageUpload["message"]["data"];
        }
        elseif ($request['slug'] == 'driver-invoice-captain-salary' || $request['slug'] == 'driver-invoice-commission-rate' ){
            $order_setting['type'] = 'driver';
            $order_setting['date_for'] = Carbon::now('utc')->format('Y-m');
            $order_setting['hash'] = '$2y$12$aWo.N4EqSLnsDICUAJuhEOHgtp4S/owBjQdPuPfu24mdIt2Nl32S.';//Nepal123$
            $order_setting['data'] = [
              $request['slug'] => $request['value']
            ];
            if($request['slug'] == 'driver-invoice-captain-salary'){
                $commission_rate = $this->settings->getSpecificSettingsBySlug('driver-invoice-commission-rate');
                if(count($commission_rate) !== 0){
                    $order_setting['data']['driver-invoice-commission-rate'] = $commission_rate[0]['value'];
                }
            }
            elseif ($request['slug'] == 'driver-invoice-commission-rate'){
                $commission_rate = $this->settings->getSpecificSettingsBySlug('driver-invoice-captain-salary');
                if(count($commission_rate) !== 0){
                    $order_setting['data']['driver-invoice-captain-salary'] = $commission_rate[0]['value'];
                }
            }
            //calling order service to store order setting
             $url = Config::get('config.order_base_url').'/order/setting';
             $storeOrderSetting = \RemoteCall::store($url,$order_setting);
             if($storeOrderSetting['status'] != '200'){
                 return response()->json([
                     'status' => $storeOrderSetting['status'],
                     'message' => $storeOrderSetting['message']['message']
                 ]);
             }
        }
        try{

            $settings = $this->settings->createSettings($request);

        }catch (\Exception $ex){
            $this->logStoreHelper->storeLogInfo(array("Settings",[
                'status'=>'404',
                'message'=>"Settings could not be created"
            ]));
            return response()->json([
                'status'=>'404',
                'message'=>"Settings could not be created"
            ],404);
        }
        DB::commit();
        $this->logStoreHelper->storeLogInfo(array("Settings",[
            'status'=>'200',
            'message'=>"Settings created successfully"
        ]));
        return response()->json([
            'status'=>'200',
            'message'=>"Settings created successfully"
        ],200);

    }

    /**
     * Edit Specific Settings
     *
     * @param $id
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function update($id, Request $request)
    {
        /**
         * Checking if the given setting with given id is available or not
         */
        try{
            $settings = $this->settings->getSpecificSettings($id);
        }catch (ModelNotFoundException $ex){
            $this->logStoreHelper->storeLogInfo(array("Settings",[
                'status'=>'404',
                'message'=>"Settings could not be found"
            ]));
            return response()->json([
                'status'=>'404',
                'message'=> "Settings could not be found"
            ],404);
        }
        if($settings['slug'] == "secret" || $settings['slug'] == "maintenance_mode"){
            return response()->json([
                'status'=>'401',
                'message'=>'This setting cannot be updated'
            ],401);
        }

        /**
         * Slugifying the slug
         */
        $request["slug"] = $this->slugify->slugify($request['slug'],['regexp' => '/([^A-Za-z]|-)+/']);
        $message =[
            "slug.required" => "The slug may not only contain numbers,special characters and should not be empty."
        ];
        /**
         * Validating the request
         */
        try{
            $this->validate($request, [
                'name'=>'required',
                'slug'=>'required|unique:settings,slug,'.$id,
                'value'=>'required',
                'type'=>'required|in:public,private',
            ],$message);
        } catch (\Exception $ex){
            $this->logStoreHelper->storeLogInfo(array("Settings",[
                'status'=>'422',
                'message'=>'Validation Error',
                'data' => $ex->response->original
            ]));
            return response()->json([
                'status'=>'422',
                'message'=>$ex->response->original
            ],422);
        }

        $request=$request->all();
        DB::beginTransaction();
        try{
            if($request['slug'] == "cover-image"){
                $image['avatar'] = $request["value"];
                $image["type"] = "logo";
                $imageUpload = \ImageUploader::upload($image);
                if ($imageUpload["status"] != 200) {
                    return response()->json(
                        $imageUpload["message"]
                        , $imageUpload["status"]);
                }
                $request['value'] = $imageUpload["message"]["data"];
            }
            elseif ($request['slug'] == 'driver-invoice-captain-salary' || $request['slug'] == 'driver-invoice-commission-rate' ){
                $order_setting['type'] = 'driver';
                $order_setting['date_for'] = Carbon::now('utc')->format('Y-m');
                $order_setting['hash'] = '$2y$12$aWo.N4EqSLnsDICUAJuhEOHgtp4S/owBjQdPuPfu24mdIt2Nl32S.';//Nepal123$
                $order_setting['data'] = [
                    $request['slug'] => $request['value']
                ];
                if($request['slug'] == 'driver-invoice-captain-salary'){
                    $commission_rate = $this->settings->getSpecificSettingsBySlug('driver-invoice-commission-rate');
                    if(count($commission_rate) !== 0){
                        $order_setting['data']['driver-invoice-commission-rate'] = $commission_rate[0]['value'];
                    }
                }
                elseif ($request['slug'] == 'driver-invoice-commission-rate'){
                    $commission_rate = $this->settings->getSpecificSettingsBySlug('driver-invoice-captain-salary');
                    if(count($commission_rate) !== 0){
                        $order_setting['data']['driver-invoice-captain-salary'] = $commission_rate[0]['value'];
                    }
                }
                //calling order service to store order setting
                $url = Config::get('config.order_base_url').'/order/setting';
                $storeOrderSetting = \RemoteCall::store($url,$order_setting);
                if($storeOrderSetting['status'] != '200'){
                    return response()->json([
                        'status' => $storeOrderSetting['status'],
                        'message' => $storeOrderSetting['message']['message']
                    ]);
                }
            }

            $this->settings->updateSettings($id, $request);

            Redis::del($settings->slug);
            $array=['flush_redis_key',$settings->slug];
            $array= serialize($array);
            Amqp::publish('', $array , [
                'exchange_type' => 'fanout',
                'exchange' => 'amq.fanout',
            ]);
//            Amqp::publish('setting_service', $array , ['queue' => 'queue2']);
        }catch (\Exception $ex){
            DB::rollBack();
            $this->logStoreHelper->storeLogInfo(array("Settings",[
                'status'=>'404',
                'message'=>"Settings could not be updated"
            ]));
            return response()->json([
                'status'=>'404',
                'message'=>"Settings could not be updated"
            ],404);
        }
        DB::commit();
        $this->logStoreHelper->storeLogInfo(array("Settings",[
            'status'=>'200',
            'message'=>"Settings updated successfully"
        ]));
        return response()->json([
            'status'=>'200',
            'message'=>"Settings updated successfully"
        ],200);
    }

    /**
     * Delete Specific Settings.
     *
     * Note: If the status of Settings is 1, deletion will change the status to 1.
     * And If the status of Settings is 0, it will delete Settings permanently.
     *
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function delete($id){
        try{
            $settings = $this->settings->getSpecificSettings($id);
        }catch (ModelNotFoundException $ex){
            $this->logStoreHelper->storeLogInfo(array("Settings",[
                'status'=>'404',
                'message'=>'Settings could not be found'
            ]));
            return response()->json([
                'status' => '404',
                'message' => "Settings could not be found"
            ], 404);
        }catch (\Exception $ex){
            $this->logStoreHelper->storeLogInfo(array("Settings",[
                'status'=>'404',
                'message'=>'Settings could not be found'
            ]));
            return response()->json([
                'status' => '404',
                'message' => "Settings could not be found"
            ], 404);
        }
        if($settings['type'] == "private"){
            return response()->json([
                'status'=>'401',
                'message'=>'This type of settings cannot be deleted'
            ],401);
        }
        DB::beginTransaction();
        try{
            $this->settings->deleteSettings($id);
            Redis::del($settings->slug);
            $array=['flush_redis_key',$settings->slug];
            $array= serialize($array);
            \Amqp::publish('', $array , [
                'exchange_type' => 'fanout',
                'exchange' => 'amq.fanout',
            ]);
        }catch (ModelNotFoundException $ex){
            DB::rollBack();
            $this->logStoreHelper->storeLogInfo(array("Settings",[
                'status'=>'404',
                'message'=>'Settings could not be found'
            ]));
            return response()->json([
                'status' => '404',
                'message' => "Settings could not be found"
            ], 404);
        }catch (\Exception $ex){
            DB::rollBack();
            $this->logStoreHelper->storeLogInfo(array("Settings",[
                'status'=>'404',
                'message'=>'Settings could not be found'
            ]));
            return response()->json([
                'status' => '404',
                'message' => "Settings could not be found"
            ], 404);
        }
        DB::commit();
        $this->logStoreHelper->storeLogInfo(array("Settings",[
            'status'=>'200',
            'message'=> "Settings Deleted Successfully"
        ]));
        return response()->json([
            'status' => '200',
            'message' => "Settings Deleted Successfully"
        ], 200);
    }

    public function updateMaintenanceMode(Request $request)
    {
        /**
         * Checking if the given setting with given id is available or not
         */
        try{
            $settings = $this->settings->getSpecificSettingsBySlug("maintenance_mode");
        }catch (ModelNotFoundException $ex){
            $this->logStoreHelper->storeLogInfo(array("Settings",[
                'status'=>'404',
                'message'=>"Settings could not be found"
            ]));
            return response()->json([
                'status'=>'404',
                'message'=> "Settings could not be found"
            ],404);
        }

        /**
         * Validating the request
         */
        try{
            $this->validate($request, [
                'value'=>'required|in:yes,no'
            ]);
        } catch (\Exception $ex){
            $this->logStoreHelper->storeLogInfo(array("Settings",[
                'status'=>'422',
                'message'=>'Validation Error',
                'data' => $ex->response->original
            ]));
            return response()->json([
                'status'=>'422',
                'message'=>$ex->response->original
            ],422);
        }

        $request=$request->all();
        DB::beginTransaction();
        try{
            $settings['value'] = strtolower($request['value']);
            $this->settings->updateSettings($settings['id'], $settings);
            Redis::del($settings->slug);
            $array=['maintenance_mode_key',$settings->slug];
            $array= serialize($array);
            Amqp::publish('', $array , [
                'exchange_type' => 'fanout',
                'exchange' => 'amq.fanout',
            ]);
//            Amqp::publish('setting_service', $array , ['queue' => 'queue2']);
        }catch (\Exception $ex){
            DB::rollBack();
            $this->logStoreHelper->storeLogInfo(array("Settings",[
                'status'=>'404',
                'message'=>"Settings could not be updated"
            ]));
            return response()->json([
                'status'=>'404',
                'message'=>"Settings could not be updated"
            ],404);
        }
        DB::commit();
        $this->logStoreHelper->storeLogInfo(array("Settings",[
            'status'=>'200',
            'message'=>"Maintenance mode is set to ".strtoupper($request['value']). " ."
        ]));
        return response()->json([
            'status'=>'200',
            'message'=>"Maintenance mode is set to ".strtoupper($request['value']). " ."
        ],200);
    }

    public function checkRedisValue($slug)
    {
        return Redis::get($slug);
    }

    public function flushRedisValue($slug)
    {
        Redis::del($slug);
        return response()->json([
            'status'=>'200',
            'message'=>"Success."
        ],200);
    }

    public function checkVersion(Request $request){
        try{
            $this->validate($request,[
                "version" => "required|string"
            ]);
        }
        catch (\Exception $ex){
            return response()->json([
                "status" => '422',
                "message" => $ex->response->original
            ],422);
        }

        try{
            $iPod    = stripos($_SERVER['HTTP_USER_AGENT'],"iPod");
            $iPhone  = stripos($_SERVER['HTTP_USER_AGENT'],"iPhone");
            $iPad    = stripos($_SERVER['HTTP_USER_AGENT'],"iPad");
            $Android = stripos($_SERVER['HTTP_USER_AGENT'],"Android");
            if( $iPod || $iPhone || $iPad ) {
                $setting = $this->settings->getSpecificSettingsBySlug("ios-version");
            }else if($Android){
                $setting = $this->settings->getSpecificSettingsBySlug("android-version");
            }
        }
        catch (\Exception $ex){

        }
    }
}
