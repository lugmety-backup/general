<?php

namespace App\Http\Controllers;

use App\Http\Facades\RemoteCallFacade;
use App\Repo\SettingsInterface;
use DrewM\MailChimp\MailChimp;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Input;

class MailChimpController extends Controller
{
    private $settings;

    public function __construct(SettingsInterface $settings)
    {
        $this->settings = $settings;
    }

    public function subscribe(Request $request)
    {
        $this->validate($request, [
            'email' => 'required|email',
            'token' => 'sometimes'
        ]);

        try{
            $mailchimp_api_keys = $this->settings->getSpecificSettingsBySlug('mailchimp-api-key');
            $mailchimp_list_ids = $this->settings->getSpecificSettingsBySlug('mailchimp-list-id');
            if(count($mailchimp_api_keys) == 0 && count($mailchimp_list_ids) == 0){
                throw new \Exception("Mailchimp Api key and List id not found");
            }
            if(count($mailchimp_api_keys) == 0){
                throw new \Exception("Mailchimp Api key not found");
            }
            if(count($mailchimp_list_ids) == 0){
                throw new \Exception("Mailchimp List id not found");
            }
        }catch (\Exception $ex){
            return response()->json([
                'status' => '404',
                'message' => $ex->getMessage()
            ], 404);
        }
        $mailchimp_api_key = $mailchimp_api_keys[0]->value;
        $list_id = $mailchimp_list_ids[0]->value;

        $url=Config::get('config.oauth_service_url');
        if($request->has('token')){
            if($request['token'] != ""){
                $result = RemoteCallFacade::getSpecificWithToken($url.'/user/details',$request['token']);
                $request['first_name']=$result['message']['data']['first_name'];
                $request['last_name']=$result['message']['data']['last_name'];
            }
        }else{
            $request['first_name']="";
            $request['last_name']="";
        }

        $client = new MailChimp($mailchimp_api_key);
        try {

            $result = $client->post("lists/$list_id/members", [
                'email_address' => $request['email'],
                'merge_fields' => ['FNAME'=>$request['first_name'], 'LNAME'=>$request['last_name']],
                'status'        => 'subscribed',
            ]);
            if($result['status']== "subscribed"){
                return response()->json([
                    'status' => '200',
                    'message' => "User subscribed successfully."
                ], 200);
            }else{
                return response()->json([
                    'status' => '200',
                    'message' => $result['title']
                ], 200);
            }

        } catch (\Exception $e) {
            return response()->json([
                'status' => '404',
                'message' => "User subscription failed"
            ], 404);
            // You can do something interesting here!
        }
    }

}