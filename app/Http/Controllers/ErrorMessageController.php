<?php

namespace App\Http\Controllers;

use App\Repo\ErrorMessageInterface;
use Bschmitt\Amqp\Facades\Amqp;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redis;
use LogStoreHelper;
use Validator;
use Cocur\Slugify\Slugify;
use Yajra\Datatables\Datatables;

/**
 * Class ErrorMessageController
 * @package App\Http\Controllers
 */
class ErrorMessageController extends Controller
{
    /**
     * @var ErrorMessageInterface
     */
    private $ErrorMessage;
    /**
     * @var LogStoreHelper
     */
    private $logStoreHelper;

    /**
     * @var Slugify
     */
    private $slugify;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(ErrorMessageInterface $ErrorMessage,
                                LogStoreHelper $logStoreHelper,
                                Slugify $slugify)
    {
        $this->ErrorMessage = $ErrorMessage;
        $this->logStoreHelper = $logStoreHelper;
        $this->slugify =$slugify;
    }

    public function indexByKey(){
        try{
            $ErrorMessage = $this->ErrorMessage->getAllErrorMessage();
            if(count($ErrorMessage) == 0){
                throw new \Exception();
            }
        }catch (\Exception $ex){
            $this->logStoreHelper->storeLogInfo(array("ErrorMessage",[
                'status'=>'404',
                'message'=>'Error Message could not be found'
            ]));
            return response()->json([
                'status' => '404',
                'message' => "Error Message could not be found"
            ], 404);
        }
        return Datatables::of($ErrorMessage)->make(true);
        $this->logStoreHelper->storeLogInfo(array("ErrorMessage",[
            'status'=>'200',
            'data'=>$ErrorMessage
        ]));
        return response()->json([
            'status' => '200',
            'data' => $ErrorMessage
        ], 200);
    }

    public function showByKey($key){
        try{
            $ErrorMessage = $this->ErrorMessage->getErrorMessageByKey($key);
            if(count($ErrorMessage) == 0){
                throw new \Exception();
            }
        }catch (\Exception $ex){
            $this->logStoreHelper->storeLogInfo(array("ErrorMessage",[
                'status'=>'404',
                'message'=>'Error Message could not be found'
            ]));
            return response()->json([
                'status' => '404',
                'message' => "Error Message could not be found"
            ], 404);
        }
        $error_data = [];
        foreach ($ErrorMessage as $message){
            $key = $message['lang'];
            $error_data[$key] = $message['value'];
        }

        $this->logStoreHelper->storeLogInfo(array("ErrorMessage",[
            'status'=>'200',
            'data'=>$error_data
        ]));
        return response()->json([
            'status' => '200',
            'data' => $error_data
        ], 200);
    }

    /**
     * Create new ErrorMessage.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        /**
         * Validating the request
         */
        try{
            $this->validate($request, [
                'key'=>'required|string',
                'translation'=>'required|array'
            ]);
        } catch (\Exception $ex){
            $this->logStoreHelper->storeLogInfo(array("ErrorMessage",[
                'status'=>'422',
                'message'=>'Validation Error',
                'data' => $ex->response->original
            ]));
            return response()->json([
                'status'=>'422',
                'message'=>$ex->response->original
            ],422);
        }
        $request=$request->all();
        DB::beginTransaction();


        try{
            $error_message = $this->ErrorMessage->deleteErrorMessageByKey($request['key']);
        }catch (\Exception $ex){
            DB::rollBack();
            $this->logStoreHelper->storeLogInfo(array("ErrorMessage",[
                'status'=>'404',
                'message'=>"Error Message could not be deleted"
            ]));
            return response()->json([
                'status'=>'404',
                'message'=>"Error Message could not be deleted."
            ],404);
        }
        $redis_data = [];
        foreach ($request['translation'] as $key=> $translation) {
            $translation['key'] = $request['key'];
            $translation['lang'] = $key;
            $rules = [
                "lang" => 'required|alpha',
                "value" => "required"
            ];
            $validator = Validator::make($translation, $rules);
            if ($validator->fails()) {
                $error = $validator->errors();
                DB::rollBack();
                $this->logStoreHelper->storeLogError(array('Page Translation', [
                    'status' => '422',
                    "message" => [$key => $error]
                ]));
                return response()->json([
                    'status' => '422',
                    "message" => [$key => $error]
                ], 422);
            }

            $redis_data[$key] = $translation['value'];

            try{
                $ErrorMessage = $this->ErrorMessage->createErrorMessage($translation);
              }catch (\Exception $ex){
                DB::rollBack();
                $this->logStoreHelper->storeLogInfo(array("ErrorMessage",[
                    'status'=>'404',
                    'message'=>"Error Message could not be created"
                ]));
                return response()->json([
                    'status'=>'404',
                    'message'=>"Error Message could not be created"
                ],404);
            }

        }
//        $redis_data = serialize($redis_data);
        Redis::del($request['key']);
        $array=['flush_redis_key',$request['key']];
        $array= serialize($array);
        Amqp::publish('', $array , [
            'exchange_type' => 'fanout',
            'exchange' => 'amq.fanout',
        ]);
//            Amqp::publish('setting_service', $array , ['queue' => 'queue2']);
        DB::commit();
        $this->logStoreHelper->storeLogInfo(array("ErrorMessage",[
            'status'=>'200',
            'message'=>"Error Message created successfully"
        ]));
        return response()->json([
            'status'=>'200',
            'message'=>"Error Message created successfully"
        ],200);

    }

    public function delete($key)
    {
        DB::beginTransaction();
        try{
            $error_message = $this->ErrorMessage->deleteErrorMessageByKey($key);
        }catch (\Exception $ex){
            DB::rollBack();
            $this->logStoreHelper->storeLogInfo(array("ErrorMessage",[
                'status'=>'404',
                'message'=>"Error Message could not be deleted"
            ]));
            return response()->json([
                'status'=>'404',
                'message'=>"Error Message could not be deleted."
            ],404);
        }catch (ModelNotFoundException $ex){
            DB::rollBack();
            $this->logStoreHelper->storeLogInfo(array("ErrorMessage",[
                'status'=>'404',
                'message'=>"Error Message could not be found"
            ]));
            return response()->json([
                'status'=>'404',
                'message'=>"Error Message could not be found."
            ],404);
        }

//        $redis_data = serialize($redis_data);
        Redis::del($key);
        $array=['flush_redis_key',$key];
        $array= serialize($array);
        Amqp::publish('', $array , [
            'exchange_type' => 'fanout',
            'exchange' => 'amq.fanout',
        ]);
//            Amqp::publish('setting_service', $array , ['queue' => 'queue2']);
        DB::commit();
        $this->logStoreHelper->storeLogInfo(array("ErrorMessage",[
            'status'=>'200',
            'message'=>"Error Message deleted successfully"
        ]));
        return response()->json([
            'status'=>'200',
            'message'=>"Error Message deleted successfully."
        ],200);

    }

}
