<?php

namespace App\Http\Controllers;

use App\Repo\EmailTemplateInterface;
use Cocur\Slugify\Slugify;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use LogStoreHelper;
use Yajra\Datatables\Facades\Datatables;
use Validator;

class EmailTemplateController extends Controller
{
    /**
     * @var EmailTemplateInterface
     */
    private $emailTemplate;
    /**
     * @var LogStoreHelper
     */
    private $logStoreHelper;

    private $slugify;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(EmailTemplateInterface $emailTemplate,
                                LogStoreHelper $logStoreHelper,
                                Slugify $slugify)
    {
        $this->emailTemplate = $emailTemplate;
        $this->logStoreHelper = $logStoreHelper;
        $this->slugify = $slugify;
    }

    /**
     * View all the Publicly Available Email Template whose status is 1
     *
     * @return \Illuminate\Http\JsonResponse
     *
     */
    public function index(Request $request){
        /**
         * limit must be integer , if limit is not given default is set to 10
         */
        $request['limit']=Input::get("limit",10);
        if (!is_null($request['limit'])) {
            $rules = [
                "limit"=>"numeric|min:1"];
            $validator = Validator::make($request->all(), $rules);

            if ($validator->fails()) {
                $request['limit']=10;
            }
        }
        try{
            $emailTemplate = $this->emailTemplate->getAllEmailTemplateForPagination($request['limit']);
            if(count($emailTemplate) == 0){
                $this->logStoreHelper->storeLogInfo(array("Email Template",[
                    'status'=>'404',
                    'message'=>'Empty Record'
                ]));
                return response()->json([
                    'status' => '404',
                    'message' => "Empty Record"
                ], 404);
            }
        }catch (\Exception $ex){
            $this->logStoreHelper->storeLogInfo(array("Email Template",[
                'status'=>'404',
                'message'=>'Email Template could not be found'
            ]));
            return response()->json([
                'status' => '404',
                'message' => "Email Template could not be found"
            ], 404);
        }
        $this->logStoreHelper->storeLogInfo(array("Email Template",[
            'status'=>'200',
            'data'=>$emailTemplate
        ]));
        return response()->json([
            'status' => '200',
            'data' => $emailTemplate
        ], 200);
    }

    /**
     * View all the Email Template with any status
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function indexAdmin(Request $request){
        /**
         * status of Email template to be displayed. If not given email template with both 1 and 0 is displayed
         */
        $status = Input::get('status');
        try{
            $emailTemplate = $this->emailTemplate->getAllEmailTemplate($status);

        }catch (\Exception $ex){
            $this->logStoreHelper->storeLogInfo(array("Email Template",[
                'status'=>'404',
                'message'=>'Email Template could not be found'
            ]));
            return response()->json([
                'status' => '404',
                'message' => "Email Template could not be found"
            ], 404);
        }
        $this->logStoreHelper->storeLogInfo(array("Email Template",[
            'status'=>'200',
            'message'=>$emailTemplate
        ]));
        return Datatables::of($emailTemplate)->make(true);
    }

    /**
     * View Specific Email Template
     *
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id){
        try{
            $emailTemplate = $this->emailTemplate->getSpecificEmailTemplate($id);
        }catch (ModelNotFoundException $ex){
            $this->logStoreHelper->storeLogInfo(array("Email Template",[
                'status'=>'404',
                'message'=>'Email Template could not be found'
            ]));
            return response()->json([
                'status' => '404',
                'message' => "Email Template could not be found"
            ], 404);
        }catch (\Exception $ex){
            $this->logStoreHelper->storeLogInfo(array("Email Template",[
                'status'=>'404',
                'message'=>'Email Template could not be found'
            ]));
            return response()->json([
                'status' => '404',
                'message' => "Email Template could not be found"
            ], 404);
        }
        $this->logStoreHelper->storeLogInfo(array("Email Template",[
            'status'=>'200',
            'message'=>$emailTemplate
        ]));
        return response()->json([
            'status' => '200',
            'message' => $emailTemplate
        ], 200);
    }

    /**
     * Create new Email Template
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        /**
         * Slugifying the slug
         */
        $request["slug"] = $this->slugify->slugify($request['slug'],['regexp' => '/([^A-Za-z]|-)+/']);
        $message =[
            "slug.required" => "The slug may not only contain numbers,special characters and should not be empty."
        ];
        /**
         * Validating the given request
         */
        try{
            $this->validate($request, [
                'title'=>'required',
                'slug'=>'required|unique:email_template',
                'header'=>'required',
                'body'=>'required',
                'status'=>'integer|min:0|max:1',
            ],$message);
        } catch (\Exception $ex){
            $this->logStoreHelper->storeLogInfo(array("Email Template",[
                'status'=>'404',
                'message'=>'Validation Error',
                'data' => $ex->response->original
            ]));
            return response()->json([
                'status'=>'422',
                'message'=>$ex->response->original
            ],422);
        }
        $request=$request->all();

        /**
         * If status is not given, default is set to 1
         */
        $request['status'] = Input::get('status', 1);
        try{
            $emailTemplate = $this->emailTemplate->getSpecificEmailTemplateBySlug($request['slug']);
            if(count($emailTemplate) != 0){
                throw new \Exception();
            }
        }catch (\Exception $ex){
            $this->logStoreHelper->storeLogInfo(array("Email Template",[
                'status'=>'409',
                'message'=> 'Email Template Already Exists'
            ]));
            return response()->json([
                'status'=>'409',
                'message'=> "Email Template Already Exists"
            ],409);
        }
        /**
         * Creating Email Template
         */
        try{
            $emailTemplate = $this->emailTemplate->createEmailTemplate($request);
        }catch (\Exception $ex){
            $this->logStoreHelper->storeLogInfo(array("Email Template",[
                'status'=>'404',
                'message'=>"Email Template could not be created"
            ]));
            return response()->json([
                'status'=>'404',
                'message'=>"Email Template could not be created"
            ],404);
        }
        $this->logStoreHelper->storeLogInfo(array("Email Template",[
            'status'=>'200',
            'message'=>"Email Template created successfully"
        ]));
        return response()->json([
            'status'=>'200',
            'message'=>"Email Template created successfully"
        ],200);

    }

    /**
     * Edit Specific Email Template
     *
     * @param $id
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function update($id, Request $request)
    {
        /**
         * Checking if the given id is valid email template or not
         */
        try{
            $emailTemplate = $this->emailTemplate->getSpecificEmailTemplate($id);
        }catch (ModelNotFoundException $ex){
            $this->logStoreHelper->storeLogInfo(array("Email Template",[
                'status'=>'404',
                'message'=>"Email Template could not be found"
            ]));
            return response()->json([
                'status'=>'404',
                'message'=> "Email Template could not be found"
            ],404);
        }

        /**
         * Slugifying the slug
         */
        $request["slug"] = $this->slugify->slugify($request['slug'],['regexp' => '/([^A-Za-z]|-)+/']);
        $message =[
            "slug.required" => "The slug may not only contain numbers,special characters and should not be empty."
        ];
        /**
         * Validating given request
         */
        try{
            $this->validate($request, [
                'title'=>'required',
                'slug'=>'required|unique:email_template,slug,'.$id,
                'header'=>'required',
                'body'=>'required',
                'status'=>'integer|min:0|max:1',
            ],$message);
        } catch (\Exception $ex){
            $this->logStoreHelper->storeLogInfo(array("Email Template",[
                'status'=>'404',
                'message'=>'Validation Error',
                'data' => $ex->response->original
            ]));
            return response()->json([
                'status'=>'422',
                'message'=>$ex->response->original
            ],422);
        }

        $request=$request->all();
        /**
         * status of email template. if not given it is set to 1.
         */
        $request['status'] = Input::get('status', 1);
        try{
            $emailTemplate = $this->emailTemplate->updateEmailTemplate($id, $request);
        }catch (\Exception $ex){
            $this->logStoreHelper->storeLogInfo(array("Email Template",[
                'status'=>'404',
                'message'=>"Email Template could not be updated"
            ]));
            return response()->json([
                'status'=>'404',
                'message'=>"Email Template could not be updated"
            ],404);
        }
        $this->logStoreHelper->storeLogInfo(array("Email Template",[
            'status'=>'200',
            'message'=>"Email Template updated successfully"
        ]));
        return response()->json([
            'status'=>'200',
            'message'=>"Email Template updated successfully"
        ],200);
    }

    /**
     * Delete Specific Email Template.
     *
     * Note: If the status of email template is 1, deletion will change the status to 1.
     * And If the status of email template is 0, it will delete email template permanently.
     *
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function delete($id){
        try{
            $emailTemplate = $this->emailTemplate->deleteEmailTemplate($id);
        }catch (ModelNotFoundException $ex){
            $this->logStoreHelper->storeLogInfo(array("Email Template",[
                'status'=>'404',
                'message'=>'Email Template could not be found'
            ]));
            return response()->json([
                'status' => '404',
                'message' => "Email Template could not be found"
            ], 404);
        }catch (\Exception $ex){
            $this->logStoreHelper->storeLogInfo(array("Email Template",[
                'status'=>'404',
                'message'=>'Email Template could not be found'
            ]));
            return response()->json([
                'status' => '404',
                'message' => "Email Template could not be found"
            ], 404);
        }
        $this->logStoreHelper->storeLogInfo(array("Email Template",[
            'status'=>'200',
            'message'=> "Email Deleted Successfully"
        ]));
        return response()->json([
            'status' => '200',
            'message' => "Email Deleted Successfully"
        ], 200);
    }
}
