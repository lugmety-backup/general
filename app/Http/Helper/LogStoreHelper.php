<?php
/**
 * Created by PhpStorm.
 * User: ashish
 * Date: 4/25/2017
 * Time: 3:05 PM
 */
use Illuminate\Support\Facades\Log;
/**
 * Class LogStoreHelper
 */
class LogStoreHelper
{
    /**
     * LogStoreHelper constructor.
     * @param \Illuminate\Http\Request $request
     */
    public function __construct(\Illuminate\Http\Request $request)
    {
        $this->user_id=RoleChecker::getUser();
    }
    /**
     * @param $data
     */
    public function storeLogInfo($data){
        if(ENV("LOG_STATUS")=="enable"){
            $data[1]['user_id']=$this->user_id;
            Log::info($data[0], $data[1]);
        }
    }
    /**
     * @param $data
     */
    public function storeLogError($data){
        if(ENV("LOG_STATUS")=="enable"){
            $data[1]['user_id']=$this->user_id;
            Log::error($data[0], $data[1]);
        }
    }
}