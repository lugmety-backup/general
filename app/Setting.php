<?php
/**
 * Created by PhpStorm.
 * User: ashish
 * Date: 4/10/2017
 * Time: 1:05 PM
 */

namespace App;

use Illuminate\Database\Eloquent\Model;

class Setting extends Model
{
    protected $guarded=['id'];
    protected $table='settings';
    protected $fillable=['name', 'slug', 'value', 'type'];
    public $enableArray = false;
    public $serializeOrNot = true;
    public function setValueAttribute($value){
        $values = explode(',', $value);
        if(count($values)>1 && $this->serializeOrNot){
            $serialized_value = serialize($values);
            $this->attributes["value"] = $serialized_value;
        }
        else{
            $this->attributes["value"] = $value;
        }
    }

    public function getValueAttribute($value){
        if($this->enableArray){
            $data = @unserialize($value);
            if ($data !== false) {
                $unserializedData = unserialize($value);
                $string = implode(",",$unserializedData);
                return $string;
            }
        }
        return $value;


    }
}