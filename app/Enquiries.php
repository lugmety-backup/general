<?php
/**
 * Created by PhpStorm.
 * User: ashish
 * Date: 4/10/2017
 * Time: 1:05 PM
 */

namespace App;

use Illuminate\Database\Eloquent\Model;

class Enquiries extends Model
{
    protected $guarded=['id'];
    protected $table='enquiries';
    protected $fillable=['full_name', 'address', 'phone', 'email_address','message'];
    protected $hidden=['updated_at'];
}