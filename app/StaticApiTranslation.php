<?php
/**
 * Created by PhpStorm.
 * User: Own
 * Date: 2/27/2018
 * Time: 8:30 PM
 */

namespace App;


use Illuminate\Database\Eloquent\Model;

class StaticApiTranslation extends Model
{
    protected $guarded=['id'];
    protected $table='static_api_translation';
    protected $fillable=['static_api_id', 'lang_code', 'title'];
    protected $hidden=['created_at','updated_at'];

    public function staticApi(){
        return $this->belongsTo('App\StaticApi','static_api_id');
    }
}