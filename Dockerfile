FROM gcr.io/lugmety-staging/lugmety-apache:v1-72

WORKDIR /var/www/app

# Copy this repo into app and composer install
ADD ./ /var/www/app/
RUN composer install && composer du -o
RUN touch /var/www/app/storage/logs/lumen.log
RUN chown -R www-data:www-data /var/www/app

# Update the default apache site with the config we created.
ADD ./docker/apache/apache-config.conf /etc/apache2/sites-enabled/000-default.conf
ADD ./docker/apache/apache2.conf /etc/supervisor/conf.d

# Start Supervisor 
CMD /usr/bin/supervisord -n -c /etc/supervisor/supervisord.conf
