<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRequestedRestaurantTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('requested_restaurant', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('restaurant_name');
            $table->string('country');
            $table->string('city');
            $table->string('district');
            $table->string('email');
            $table->string('phone',19);
            $table->text('request_letter');
            $table->timestamps();
            $table->engine = "InnoDB";

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('requested_restaurant');
    }
}
