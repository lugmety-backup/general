<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStaticBlockTranslationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('static_block_translation', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('static_block_id');
            $table->foreign('static_block_id')->references('id')->on('static_block')->onDelete('cascade');
            $table->string('lang_code', 20);
            $table->text('title');
            $table->text('content');
            $table->timestamps();
            $table->engine = "InnoDB";
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('static_block_translation');
    }
}
