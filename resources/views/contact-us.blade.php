@extends('master')
@section('content')
    <table class="row mb30" style="border-collapse:collapse;border-spacing:0;display:table;margin-top:30px;margin-bottom:30px!important;padding:0;position:relative;text-align:left;vertical-align:top;width:100%">
        <tbody>
        <tr style="padding:0;text-align:left;vertical-align:top">
            <th class="small-12 large-12 columns first last" style="Margin:0 auto;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0 auto;padding:0!important;padding-bottom:16px;padding-left:16px;padding-right:16px;text-align:left;width:564px">
                <table style="border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%">
                    <tr style="padding:0;text-align:left;vertical-align:top">
                        <th style="Margin:0;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0;padding:0;text-align:left">
                            <center data-parsed="" style="min-width:532px;width:100%">
                                <p class="message float-center" align="center" style="Margin:0;Margin-bottom:10px;color:#0a0a0a;font-family:Montserrat,sans-serif!important;font-size:15px!important;font-weight:400!important;line-height:1.3;margin:0!important;margin-bottom:10px;padding:5px 20px!important;text-align:center!important">
                                    Dear Lugmety,<br>

                                    {{ $contactUs["message"] }}
                                    <br>

                                    Thank You.
                                </p>
                            </center>
                        </th>
                        <th class="expander" style="Margin:0;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0;padding:0!important;text-align:left;visibility:hidden;width:0"></th>
                    </tr>
                </table>
            </th>
        </tr>
        </tbody>
    </table>
    <table class="row mb30" style="border-collapse:collapse;border-spacing:0;display:table;margin-bottom:30px!important;padding:0;position:relative;text-align:left;vertical-align:top;width:100%">
        <tbody>
        <tr style="padding:0;text-align:left;vertical-align:top">
            <th class="small-12 large-12 columns first last" style="Margin:0 auto;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0 auto;padding:0!important;padding-bottom:16px;padding-left:16px;padding-right:16px;text-align:left;width:564px">
                <table style="border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%">
                    <tr style="padding:0;text-align:left;vertical-align:top">
                        <th style="Margin:0;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0;padding:0;text-align:left">
                            <div class="table-wrapper" style="border:1px solid #ddd;border-top:none">
                                <table class="row" style="border-collapse:collapse;border-spacing:0;display:table;padding:0;position:relative;text-align:left;vertical-align:top;width:100%">
                                    <tbody>
                                    <tr style="padding:0;text-align:left;vertical-align:top">
                                        <th class="small-12 large-12 columns first last" style="Margin:0 auto;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0 auto;padding:0!important;padding-bottom:16px;padding-left:0!important;padding-right:0!important;text-align:left;width:100%">
                                            <table style="border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%">
                                                <tr style="padding:0;text-align:left;vertical-align:top">
                                                    <th style="Margin:0;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0;padding:0;text-align:left">
                                                        <div class="title" style="background: #bf9f60!important;border-bottom: 1px solid #bf9f60;border-top: 1px dashed #bf9f60;font-weight:700;padding:10px!important;color: #fff;">Sender Details:</div>
                                                    </th>
                                                    <th class="expander" style="Margin:0;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0;padding:0!important;text-align:left;visibility:hidden;width:0"></th>
                                                </tr>
                                            </table>
                                        </th>
                                    </tr>
                                    </tbody>
                                </table>
                                <table class="row" style="border-collapse:collapse;border-spacing:0;display:table;padding:0;position:relative;text-align:left;vertical-align:top;width:100%">
                                    <tbody>
                                    <tr style="padding:0;text-align:left;vertical-align:top">
                                        <th class="small-12 large-6 columns first" style="Margin:0 auto;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0 auto;padding:0!important;padding-bottom:16px;padding-left:0!important;padding-right:0!important;text-align:left;width:50%">
                                            <table style="border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%">
                                                <tr style="padding:0;text-align:left;vertical-align:top">
                                                    <th style="Margin:0;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0;padding:0;text-align:left">
                                                        <p style="Margin:0;Margin-bottom:10px;color:#0a0a0a;font-family:Montserrat,sans-serif!important;font-size:12px!important;font-weight:400!important;line-height:1.3;margin:0!important;margin-bottom:10px;padding:5px 20px!important;text-align:left">Name: {{$contactUs["full_name"] }}</p>
                                                        <p style="Margin:0;Margin-bottom:10px;color:#0a0a0a;font-family:Montserrat,sans-serif!important;font-size:12px!important;font-weight:400!important;line-height:1.3;margin:0!important;margin-bottom:10px;padding:5px 20px!important;text-align:left">Address: {{$contactUs["address"] }}</p>
                                                        <p style="Margin:0;Margin-bottom:10px;color:#0a0a0a;font-family:Montserrat,sans-serif!important;font-size:12px!important;font-weight:400!important;line-height:1.3;margin:0!important;margin-bottom:10px;padding:5px 20px!important;text-align:left">Phone: {{$contactUs["phone"] }}</p>
                                                        <p style="Margin:0;Margin-bottom:10px;color:#0a0a0a;font-family:Montserrat,sans-serif!important;font-size:12px!important;font-weight:400!important;line-height:1.3;margin:0!important;margin-bottom:10px;padding:5px 20px!important;text-align:left">Email Address: {{$contactUs["email"] }}</p>
                                                    </th>
                                                </tr>
                                            </table>
                                        </th>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </th>
                    </tr>
                </table>
            </th>
        </tr>
        </tbody>
    </table>

    </td>
    </tr>
    </tbody>
    </table>
    {{--<table class="row footer-table" style="border-collapse:collapse;border-spacing:0;padding:0;position:relative;text-align:left;vertical-align:top;width:100%">--}}
        {{--<tbody>--}}
        {{--<tr style="padding:0;text-align:left;vertical-align:top">--}}
            {{--<th class="small-12 large-12 columns first last" style="Margin:0 auto;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0 auto;padding:0!important;padding-bottom:16px;padding-left:16px;padding-right:16px;text-align:left;width:564px">--}}
                {{--<table style="border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%">--}}
                    {{--<tr style="padding:0;text-align:left;vertical-align:top">--}}
                        {{--<th style="Margin:0;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0;padding:0;text-align:left">--}}
                            {{--<table class="row table-head" style="background:#e1b67c url(http://lugmety.go-demo.com/lugmety/images/bg-pattern.png) repeat 0 0;border-bottom:10px solid #c79d66!important;border-collapse:collapse;border-spacing:0;border-top:none!important;padding:0;position:relative;text-align:left;vertical-align:top;width:100%">--}}
                                {{--<tbody>--}}
                                {{--<tr style="padding:0;text-align:left;vertical-align:top">--}}
                                    {{--<th class="small-12 large-12 columns first last" style="Margin:0 auto;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0 auto;padding:0!important;padding-bottom:16px;padding-left:0!important;padding-right:0!important;text-align:left;width:100%">--}}
                                        {{--<table style="border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%">--}}
                                            {{--<tr style="padding:0;text-align:left;vertical-align:top">--}}
                                                {{--<th style="Margin:0;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0;padding:0;text-align:left">--}}
                                                    {{--<p class="footer-text" style="Margin:0;Margin-bottom:10px;color:#fff!important;font-family:Montserrat,sans-serif!important;font-size:16px!important;font-weight:400!important;line-height:1.3;margin:0!important;margin-bottom:10px;padding:20px!important;text-align:center!important">Thank You Again!</p>--}}
                                                {{--</th>--}}
                                                {{--<th class="expander" style="Margin:0;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0;padding:0!important;text-align:left;visibility:hidden;width:0"></th>--}}
                                            {{--</tr>--}}
                                        {{--</table>--}}
                                    {{--</th>--}}
                                {{--</tr>--}}
                                {{--</tbody>--}}
                            {{--</table>--}}
                        {{--</th>--}}
                    {{--</tr>--}}
                {{--</table>--}}
@stop