<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$app->get("/",function (){
    return response()->json([
        'success' => true
    ]);
});

$app->group(['Email Template route group'], function () use ($app) {

    $app->get('public/email/template',[
        'uses' => 'EmailTemplateController@index'
    ]);

    $app->get('admin/email/template',['middleware' => 'checkTokenPermission:view-email-template',
        'uses' => 'EmailTemplateController@indexAdmin'
    ]);

    $app->get('email/template/{id:[0-9]+}',[
        'uses' => 'EmailTemplateController@show'
    ]);

    $app->post('email/template',[/*'middleware' => 'checkTokenPermission:create-email-template',*/
        'uses' => 'EmailTemplateController@store'
    ]);

    $app->put('email/template/{id:[0-9]+}',['middleware' => 'checkTokenPermission:edit-email-template',
        'uses' => 'EmailTemplateController@update'
    ]);

    $app->delete('email/template/{id:[0-9]+}',['middleware' => 'checkTokenPermission:delete-email-template',
        'uses' => 'EmailTemplateController@delete'
    ]);

});

$app->group(['Page route group'], function () use ($app) {

    $app->get('cms/public/page',[
        'uses' => 'PageController@index'
    ]);

    $app->get('cms/admin/page',['middleware' => 'checkTokenPermission:view-page',
        'uses' => 'PageController@indexAdmin'
    ]);

    $app->get('cms/public/page/{id:[0-9]+}',[
        'uses' => 'PageController@show'
    ]);

    $app->get('cms/public/page/{country_code}/{slug}',[
        'uses' => 'PageController@showBySlug'
    ]);

    $app->get('cms/admin/page/{id:[0-9]+}',['middleware' => 'checkTokenPermission:view-page',
        'uses' => 'PageController@adminShow'
    ]);

    $app->post('cms/page',['middleware' => 'checkTokenPermission:create-page',
        'uses' => 'PageController@store'
    ]);

    $app->put('cms/page/{id:[0-9]+}',['middleware' => 'checkTokenPermission:edit-page',
        'uses' => 'PageController@update'
    ]);

    $app->delete('cms/page/{id:[0-9]+}',['middleware' => 'checkTokenPermission:delete-page',
        'uses' => 'PageController@delete'
    ]);

});

$app->group(['Static Block route group'], function () use ($app) {

    $app->get('cms/public/block',[
        'uses' => 'StaticBlockController@index'
    ]);

    $app->get('cms/admin/block',['middleware' => 'checkTokenPermission:view-static-block',
        'uses' => 'StaticBlockController@indexAdmin'
    ]);

    $app->get('cms/public/block/{id:[0-9]+}',[
        'uses' => 'StaticBlockController@show'
    ]);

    $app->get('cms/public/block/{country_code}/{slug}',[
        'uses' => 'StaticBlockController@showBySlug'
    ]);

    $app->get('cms/admin/block/{id:[0-9]+}',['middleware' => 'checkTokenPermission:view-static-block',
        'uses' => 'StaticBlockController@adminShow'
    ]);

    $app->post('cms/block',['middleware' => 'checkTokenPermission:create-static-block',
        'uses' => 'StaticBlockController@store'
    ]);

    $app->put('cms/block/{id:[0-9]+}',['middleware' => 'checkTokenPermission:edit-static-block',
        'uses' => 'StaticBlockController@update'
    ]);

    $app->delete('cms/block/{id:[0-9]+}',['middleware' => 'checkTokenPermission:delete-static-block',
        'uses' => 'StaticBlockController@delete'
    ]);

});


$app->group(['Static Api route group'], function () use ($app) {

    $app->get('static-api/{countryId:[0-9]+}/{value}',[
        'uses' => 'StaticApiController@getSpecificStaticApi'
    ]);



});

$app->group(['Setting route group'], function () use ($app) {

    $app->get('admin/settings',['middleware' => 'checkTokenPermission:view-settings',
        'uses' => 'SettingsController@indexAdmin'
    ]);

    $app->get('public/settings',[
        'uses' => 'SettingsController@indexPublic'
    ]);

    $app->get('check/version',[
        'uses' => 'SettingsController@checkVersion'
    ]);

    $app->get('public/settings/{slug}',[
        'uses' => 'SettingsController@show'
    ]);

    $app->get('admin/settings/{id}',['middleware' => 'checkTokenPermission:view-settings',
        'uses' => 'SettingsController@showAdmin'
    ]);

    $app->post('settings',['middleware' => 'checkTokenPermission:create-settings',
        'uses' => 'SettingsController@store'
    ]);

    $app->put('settings/{id:[0-9]+}',['middleware' => 'checkTokenPermission:edit-settings',
        'uses' => 'SettingsController@update'
    ]);

    $app->delete('settings/{id:[0-9]+}',['middleware' => 'checkTokenPermission:delete-settings',
        'uses' => 'SettingsController@delete'
    ]);

    $app->post('settings/maintenance/mode',['middleware' => 'checkTokenPermission:create-settings',
        'uses' => 'SettingsController@updateMaintenanceMode'
    ]);

    $app->post('redis/slug/{slug}',['middleware' => 'checkTokenPermission:create-settings',
        'uses' => 'SettingsController@checkRedisValue'
    ]);

    $app->delete('redis/slug/{slug}',[
        'uses' => 'SettingsController@flushRedisValue'
    ]);

});

$app->group(['Error Message route group'], function () use ($app) {

    $app->post('error-message',[/*'middleware' => 'checkTokenPermission:create-settings',*/
        'uses' => 'ErrorMessageController@store'
    ]);

    $app->get('error-message/{key}',[/*'middleware' => 'checkTokenPermission:create-settings',*/
        'uses' => 'ErrorMessageController@showByKey'
    ]);

    $app->get('error-message',[/*'middleware' => 'checkTokenPermission:create-settings',*/
        'uses' => 'ErrorMessageController@indexByKey'
    ]);

    $app->delete('error-message/{key}',[/*'middleware' => 'checkTokenPermission:create-settings',*/
        'uses' => 'ErrorMessageController@delete'
    ]);

});

$app->group(['Enquiry route group'], function () use ($app) {

    $app->get('admin/enquiry',['middleware' => 'checkTokenPermission',
        'uses' => 'EnquiriesController@index'
    ]);

    $app->get('admin/enquiry/{id}',[
        'uses' => 'EnquiriesController@show'
    ]);

    $app->post('public/enquiry',[
        'uses' => 'EnquiriesController@store'
    ]);

    $app->delete('admin/enquiry/{id:[0-9]+}',['middleware' => 'checkTokenPermission',
        'uses' => 'EnquiriesController@delete'
    ]);

});

$app->group(['restaurant/request route group'], function () use ($app) {

    $app->get('admin/restaurant/request', ['middleware' => 'checkTokenPermission',
        'uses' => 'RequestedRestaurantController@index'
    ]);

    $app->get('admin/restaurant/count/request', ['middleware' => 'checkTokenPermission',
        'uses' => 'RequestedRestaurantController@countUnseen'
    ]);

    $app->get('admin/restaurant/request/{id}', [
        'uses' => 'RequestedRestaurantController@show'
    ]);

    $app->post('public/restaurant/request', [
        'uses' => 'RequestedRestaurantController@store'
    ]);

    $app->delete('admin/restaurant/request/{id:[0-9]+}', ['middleware' => 'checkTokenPermission',
        'uses' => 'RequestedRestaurantController@delete'
    ]);
});

$app->group(['Contact Us'], function () use ($app) {

    $app->post('contact/us',[
//        'middleware' => 'checkTokenPermission',
        'uses' => 'ContactUsController@sendMessage'
    ]);

//    $app->get('shipping/method',['middleware' => 'checkTokenPermission',
//        'uses' => 'ShippingMethodController@index'
//    ]);
//
//    $app->post('shipping/method',['middleware' => 'checkTokenPermission',
//        'uses' => 'ShippingMethodController@store'
//    ]);
//
//    $app->put('shipping/method/{id}',['middleware' => 'checkTokenPermission',
//        'uses' => 'ShippingMethodController@update'
//    ]);
//
//    $app->delete('shipping/method/{id}',['middleware' => 'checkTokenPermission',
//        'uses' => 'ShippingMethodController@delete'
//    ]);

});

$app->get('/publish', function () use ($app) {
    \Amqp::publish('', 'prashant' , [
        'exchange_type' => 'fanout',
        'exchange' => 'amq.fanout',
    ]);
    echo "published";
});

$app->get('connection/checker', 'ConnectionController@index');

$app->post('newsletter/subscribe',['as'=>'subscribe','uses'=>'MailChimpController@subscribe']);

$app->get('redis/slug/{slug}', function ($slug) use ($app) {
    return \Redis::get($slug);
});

$app->get('contact-us', function () {
    return view('contact-us');
});
