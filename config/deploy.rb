# config valid only for current version of Capistrano
lock "3.11.0"

set :application, "lugmety_general"
set :repo_url, 'git@gitlab.com:lugmety/shipping-service.git'
