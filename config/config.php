<?php
/**
 * Created by PhpStorm.
 * User: ashish
 * Date: 4/10/2017
 * Time: 3:35 PM
 */
$app_base_url = env('API_BASE_URL');
$cdn_base_url = env('CDN_BASE_URL');

    return array(
        'url' => $app_base_url.'/auth/v1/oauth/introspect',
        //'url'=>'http://localhost:8888/oauth/introspect',
        // 'access_token'=>"eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6IjA0YjM4YmRmZDRlNWJlMDJjZGE3MjU4YjFkYjFmOGExOTViYjkzNjMwODQzYjZhNTg5ZTU2YWViYTc2NDgxNDNkNDRmZTUzZmFiZjI0Njg5In0.eyJhdWQiOiI1IiwianRpIjoiMDRiMzhiZGZkNGU1YmUwMmNkYTcyNThiMWRiMWY4YTE5NWJiOTM2MzA4NDNiNmE1ODllNTZhZWJhNzY0ODE0M2Q0NGZlNTNmYWJmMjQ2ODkiLCJpYXQiOjE0OTE1NDA2NzEsIm5iZiI6MTQ5MTU0MDY3MSwiZXhwIjoxNTIzMDc2NjcxLCJzdWIiOiIxIiwic2NvcGVzIjpbXX0.bL9_FaUiC7qYRuWkG4Bz4foWaWhFTxZs4cKZYRBNIQEdyg8XN6qG_XHuGe38J8FmD4MWPBlQ2MF7kqt7c9tg1bghH7l8uaaFCYdb5tidmeG_texh_GpekmEirkyLwMGkfbv_hXPb5RyXO6F4wNO0Z9xlUgC451vy6gE0W2E8swQb_-Oih6O9rC7S6ahnA0P3NR-npwL6_8YrGD0A_qvlGfWcg1zCveW7nzNp4WQAdI898NWDModP7yRJgCasHLLnGtI8KlKuiyZC_Owitjjj36F3SVid61v0i5PN_Ux3GTv9NqHpK8Ktxw6Yg5Q-w7dBwQK0IAMuejaN_uMM3eZrLTTPBRDBTAmSH-b-5INu7qDvOqv-0F0mjlXvgKvUPLwDRP1uNe8UT1xUx_5JWLJJzn5Ca-yQUT76s7arPdY6jFfezg8pi8_t-6csBYi_FcXGQnsW5pb12aNK46I5Kzuh1TL1VXtG38WICm27fa-Hj12rHJr66N8kUIb1_txtZOSUghABf1rvE0z9CYZw5JbKQkC0zQtL_YjJlRUOXFV8urzkrPLcrtmLQffnoJe0ol4inPM_863xoWyptZJU6QbTvAqplCfTjd_boxEcw-7K-cCqJEHVb9tUyajgk5E-fQMZax6Mm3p18TNucP8RVBiAl1WL9qUJvocKw9GeLVqG0jM",
        'IMAGE_BASE_PATH' => '/public/images/',
        'image_service_base_url' => $app_base_url.'/cdn/v1',//dont add / after v1
        "image_service_base_url_cdn" => $cdn_base_url,//cdn base url for image display
        'oauth_service_url' => $app_base_url.'/auth/v1',//dont add / after v1
        'order_base_url' =>  $app_base_url.'/order/v1',
    );

